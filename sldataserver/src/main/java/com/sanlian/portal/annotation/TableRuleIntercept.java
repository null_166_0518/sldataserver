package com.sanlian.portal.annotation;

import java.lang.annotation.*;

/**
 * 
 * @描述:TODO (分表拦截器)
 * @ClassName: TableRuleIntercept
 * @author yujian
 * @date 2019年4月24日
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})  
@Retention(RetentionPolicy.RUNTIME)  
@Documented
public @interface TableRuleIntercept {
}
