package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.sanlian.portal.datasource.ShardingConfiguration;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.entity.SLDeferredResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Resource;

/**
 * @ClassName TransactionTaskManager
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/8
 **/
@Component
public class TransactionTaskManager {
    @Resource
    TransactionTemplate txTemplate;
    @Resource
    JdbcTemplate jdbcTemplate;
    @Resource
    SlaveTaskManager slaveTaskManager;
    @Autowired
    ShardingConfiguration shardingConfiguration;

    //@Transactional(propagation = Propagation.REQUIRES_NEW,transactionManager = "capabilityDataSourceTransactionManager")
    public DeferredResult<String> exeTask(TMethodTypeEnum methodTypeEnum, String param) {
        JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
        //System.out.println(jsonBody.getKeyNum());
        SLDeferredResult<String> deferredResult = TaskRunnableFactory.watchRequests.get(jsonBody.getKeyNum());
        if (deferredResult == null) {
            deferredResult = new SLDeferredResult<String>(shardingConfiguration.getTransactionTimeout(),System.currentTimeMillis());
            //deferredResult.setResult(JSON.toJSONString(ResponseHelper.buildResponseResult("0", "请勿重复请求", "")));
            TaskRunnableFactory.watchRequests.put(jsonBody.getKeyNum(),deferredResult);
        }

        //设置超时函数
        deferredResult.onTimeout(new TimeoutCallback(jsonBody.getKeyNum(),deferredResult));
        Runnable runnable;
        //如果是双机
        if (shardingConfiguration.getSingleOrDouble().equals("2")) {
            //查询操作且主库模式，查询转为从库读取
            if (methodTypeEnum == TMethodTypeEnum.Select) {
                runnable = slaveTaskManager.getSlaveTask(methodTypeEnum, deferredResult, jsonBody);
                TaskRunnableFactory.es.execute(runnable);
                deferredResult.onCompletion(new ResultCallback(jsonBody));
                return deferredResult;

            } else {
                if (shardingConfiguration.getSingletonMasterToSlave().equals("1")) {
                    //进行从库同步
                    deferredResult.onCompletion(new ResultCallback(slaveTaskManager, methodTypeEnum, jsonBody,deferredResult));
                }
            }
        }
        //判断是主库读写还是从读写
        runnable = shardingConfiguration.getSingletonMasterToSlave().equals("1") ? getMasterTask(methodTypeEnum, deferredResult, jsonBody) : slaveTaskManager.getSlaveTask(methodTypeEnum, deferredResult, jsonBody);
        TaskRunnableFactory.es.execute(runnable);
        return deferredResult;
    }

    public Runnable getMasterTask(TMethodTypeEnum methodTypeEnum, SLDeferredResult<String> deferredResult, JsonBody jsonBody) {

        return TaskRunnableFactory.getRunnable(shardingConfiguration, jdbcTemplate, txTemplate, methodTypeEnum, deferredResult, jsonBody);
    }
}
