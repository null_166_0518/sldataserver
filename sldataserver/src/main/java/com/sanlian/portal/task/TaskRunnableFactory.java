package com.sanlian.portal.task;

import com.sanlian.portal.datasource.ShardingConfiguration;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.entity.SLDeferredResult;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName TaskRunnableFactory
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/12
 **/
public class TaskRunnableFactory {
    // 创建线程池
    public static ExecutorService es = Executors.newCachedThreadPool();

    public static ConcurrentHashMap<String, SLDeferredResult<String>> watchRequests =new ConcurrentHashMap<String, SLDeferredResult<String>>();

    public static Runnable getRunnable(ShardingConfiguration shardingConfiguration,JdbcTemplate jdbcTemplate, TransactionTemplate txTemplate, TMethodTypeEnum methodTypeEnum, DeferredResult<String> deferredResult, JsonBody jsonBody) {
//        if (shardingConfiguration.getTables().size()>0){
//          String sql=jsonBody.getCommandStr();
//            for (Map.Entry<String, List<Integer>> entry : shardingConfiguration.getTables().entrySet()) {
//                String mapKey = entry.getKey();
//                List<Integer> mapValue = entry.getValue();
//                if (sql.indexOf(mapKey)!=-1){
//                  int months= Integer.parseInt(Common.fromDateM());
//                    if (mapValue.contains(months)){
//                        String tableName=mapKey+"_"+months;
//                        jsonBody.setCommandStr(StringUtils.replace(sql,mapKey,tableName));
//                    }
//                }
//            }
//        }
        Runnable runnable = null;
        switch (methodTypeEnum) {
            case Select:
                runnable = new SelectRunnable(jdbcTemplate, deferredResult, jsonBody);
                break;
            case Insert:
                runnable = new InsertRunnable(jdbcTemplate, deferredResult, jsonBody);
                break;
            case Update:
                runnable = new UpdateRunnable(jdbcTemplate, deferredResult, jsonBody);
                break;
            case Delete:
                runnable = new DeleteRunnable(jdbcTemplate, deferredResult, jsonBody);
                break;
            case DBTransaction:
                runnable = new DBTransactionRunnable(jdbcTemplate, txTemplate, deferredResult, jsonBody);
                break;
            case Callable:
                runnable = new CallableRunnable(jdbcTemplate, deferredResult, jsonBody);
                break;

        }
        return runnable;
    }
}
