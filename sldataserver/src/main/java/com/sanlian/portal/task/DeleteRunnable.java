package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.util.StringUtil;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.entity.SLDeferredResult;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName DeleteRunnable
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/9
 **/
public class DeleteRunnable implements Runnable {
    JsonBody jsonBody;
    JdbcTemplate jdbcTemplate;
    DeferredResult<String> deferredResult;

    public DeleteRunnable(JdbcTemplate jdbcTemplate, DeferredResult<String> deferredResult, JsonBody jsonBody) {
        this.jdbcTemplate = jdbcTemplate;
        this.deferredResult = deferredResult;
        this.jsonBody = jsonBody;
    }

    @Override
    public void run() {

        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
            //System.out.println(jsonBody.getCommandStr());
            try {
                String[] sql = jsonBody.getCommandStr().split("\\#");
                //jdbcTemplate.execute(jsonBody.getCommandStr());
                jdbcTemplate.batchUpdate(sql);
            } catch (Exception e) {
                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
                deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage())));
            }

        }
        deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS)));
    }
}

