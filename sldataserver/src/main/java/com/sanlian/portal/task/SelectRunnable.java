package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.util.StringUtil;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.datasource.DataSourceKey;
import com.sanlian.portal.datasource.DynamicDataSourceContextHolder;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName SelectRunnable
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/9
 **/
public class SelectRunnable implements Runnable {
    JsonBody jsonBody;
    JdbcTemplate jdbcTemplate;
    DeferredResult<String> deferredResult;

    public SelectRunnable(JdbcTemplate jdbcTemplate, DeferredResult<String> deferredResult, JsonBody jsonBody) {
        this.jdbcTemplate = jdbcTemplate;
        this.deferredResult = deferredResult;
        this.jsonBody = jsonBody;
    }

    @Override
    public void run() {
        exeSelect();
    }
    private void exeSelect(){

        List<Map<String, Object>> list = new ArrayList<>();
        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
            try {
                // 通过jdbcTemplate查询数据库
                list = jdbcTemplate.queryForList(jsonBody.getCommandStr());
            } catch (Exception e) {
                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
                deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage(), "")));
            }

        }
        if (list.size() > 0) {
            String reult = JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(list.size()), list), SerializerFeature.WriteMapNullValue);
            //long time2 = System.currentTimeMillis();
            //System.out.println((time2 - time1) + "=======" + reult);
            deferredResult.setResult(reult);
        } else {
            deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("2", "0", "")));
        }
    }
}

