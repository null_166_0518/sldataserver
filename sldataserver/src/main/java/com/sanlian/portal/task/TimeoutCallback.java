package com.sanlian.portal.task;

import org.springframework.web.context.request.async.DeferredResult;

/**
 * @ClassName TimeoutCallback
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/11
 **/
public class TimeoutCallback implements Runnable {
    DeferredResult<String> deferredResult;
    String keyNum;

    public TimeoutCallback(String keyNum, DeferredResult<String> deferredResult) {
        this.deferredResult = deferredResult;
        this.keyNum = keyNum;
    }

    @Override
    public void run() {
        System.out.println("异步线程执行超时");
        deferredResult.setResult("线程执行超时");
        TaskRunnableFactory.watchRequests.remove(keyNum);
    }
}
