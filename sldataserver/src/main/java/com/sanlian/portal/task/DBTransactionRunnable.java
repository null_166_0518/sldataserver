package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.util.StringUtil;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.entity.DBColumnTypeEnum;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.request.async.DeferredResult;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @ClassName DBTransactionRunnable
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/9
 **/
public class DBTransactionRunnable implements Runnable {
    JsonBody jsonBody;
    JdbcTemplate jdbcTemplate;
    TransactionTemplate txTemplate;
    DeferredResult<String> deferredResult;

    public DBTransactionRunnable(JdbcTemplate jdbcTemplate, TransactionTemplate txTemplate, DeferredResult<String> deferredResult, JsonBody jsonBody) {
        this.jdbcTemplate = jdbcTemplate;
        this.txTemplate = txTemplate;
        this.deferredResult = deferredResult;
        this.jsonBody = jsonBody;
    }

    @Override
    public void run() {
        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
            String[] sqls = jsonBody.getCommandStr().split(";");
            Object res = txTemplate.execute(new TransactionCallback<Object>() {
                @Override
                public Object doInTransaction(TransactionStatus transactionStatus) {
                    //Object savepoint = transactionStatus.createSavepoint();
                    // DML执行
                    JSONArray array = JSON.parseArray(jsonBody.getCommandBlob());
                    try {
                        for (int j = 0; j < sqls.length; j++) {
                            if (array != null) {
                                JSONArray ar = (JSONArray) array.get(j);
                                jdbcTemplate.update(sqls[j], new PreparedStatementSetter() {
                                    @Override
                                    public void setValues(PreparedStatement ps) throws SQLException {
                                        if (ar != null) {
                                            int size = ar.size() + 1;
                                            for (int i = 1; i < size; i++) {
                                                JSONObject jso = (JSONObject) ar.get(i - 1);
                                                String base64 = "";
                                                try {
                                                    // base64 = URLDecoder.decode(jso.get("BLOB").toString(), "utf-8");
                                                    base64 = jso.get(DBColumnTypeEnum.BLOB.name()).toString();
                                                    if (StringUtil.isNotEmpty(base64)) {
                                                        byte[] bs = new Base64().decode(base64);
                                                        ps.setObject(i, bs);
                                                    } else {
                                                        ps.setObject(i, new byte[0]);
                                                    }
                                                } catch (Exception e) {
                                                    LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, jso.get("BLOB").toString() + e.getMessage(), e);
                                                    ps.setObject(i, new byte[0]);
                                                }

                                            }
                                        }

                                    }
                                });
                            } else {
                                jdbcTemplate.update(sqls[j]);
                            }
                        }
                    } catch (Exception e) {
                        LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
                        transactionStatus.setRollbackOnly();
                        // transactionStatus.rollbackToSavepoint(savepoint);
                        return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage()));
                    }

                    return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS));
                }
            });
            //System.out.println(String.valueOf(res));
            deferredResult.setResult(String.valueOf(res));
        }
        deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", "没有sql语句")));
    }
}

