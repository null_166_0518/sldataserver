package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.util.StringUtil;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.datasource.DataSourceKey;
import com.sanlian.portal.datasource.DynamicDataSourceContextHolder;
import com.sanlian.portal.entity.DBColumnTypeEnum;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.exception.SystemException;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import org.apache.commons.codec.binary.Base64;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.web.context.request.async.DeferredResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName InsertRunnable
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/9
 **/
public class InsertRunnable implements Runnable {
    JsonBody jsonBody;
    JdbcTemplate jdbcTemplate;
    DeferredResult<String> deferredResult;

    public InsertRunnable(JdbcTemplate jdbcTemplate, DeferredResult<String> deferredResult, JsonBody jsonBody) {
        this.jdbcTemplate = jdbcTemplate;
        this.deferredResult = deferredResult;
        this.jsonBody = jsonBody;
//        deferredResult.onTimeout(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("异步线程执行超时");
//                deferredResult.setResult("线程执行超时");
//            }
//        });
//        deferredResult.onCompletion(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("异步执行完毕");
//                DynamicDataSourceContextHolder.setDataSource(DataSourceKey.slave.name());
//                exeInsert();
//                if (DataSourceKey.slave.name().equals( DynamicDataSourceContextHolder.getDataSource())) {
//                    DynamicDataSourceContextHolder.clearDataSource();
//                }
//            }
//        });
    }

    @Override
    public void run() {
        exeInsert();
    }

    private void exeInsert() {
        //String result = Constans.SUCCESS;
        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
            System.out.println(jsonBody.getCommandStr());
            try {
                JSONArray array = JSON.parseArray(jsonBody.getCommandBlob());
                //int size = array.size();
//                KeyHolder keyHolder = new GeneratedKeyHolder();
//                Connection connection1 = jdbcTemplate.getDataSource()
//                        .getConnection();

                if (array != null) {
                    jdbcTemplate.update(new PreparedStatementCreator() {
                        @Override
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                            try {
                                PreparedStatement ps = connection.prepareStatement(jsonBody.getCommandStr());// Statement.RETURN_GENERATED_KEYS);, new String[]{"id"}
                                int size = array.size() + 1;
                                for (int i = 1; i < size; i++) {
                                    JSONObject jso = (JSONObject) array.get(i - 1);
                                    String key = jso.keySet().iterator().next();

                                    DBColumnTypeEnum dbColumnTypeEnum = DBColumnTypeEnum.valueOf(key.toUpperCase());
                                    switch (dbColumnTypeEnum) {
                                        case INT:
                                            ps.setInt(i, jso.getIntValue(key));
                                            break;
                                        case STRING:
                                            ps.setString(i, jso.getString(key));
                                            break;
                                        case BLOB:
                                            String base64 = jso.get(key).toString();
                                            if (StringUtil.isNotEmpty(base64)) {
                                                byte[] bs = new Base64().decode(base64.replaceAll(" ", ""));
                                                ps.setObject(i, bs);
                                            } else {
                                                ps.setObject(i, new byte[0]);
                                            }
                                            break;
                                        case LONG:
                                            ps.setLong(i, jso.getLong(key));
                                            break;
                                        case DATE:
                                            ps.setTimestamp(i, jso.getTimestamp(key));
                                            break;
                                    }
                                }
                                return ps;
                            } catch (SQLException e) {
                                //deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage())));
                                throw new SQLException(e);
                            }
                        }


                    });
                    //System.out.println("id:" + keyHolder.getKey());
                    //return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(keyHolder.getKey())));
                } else {
                    jdbcTemplate.update(new PreparedStatementCreator() {
                        @Override
                        public PreparedStatement createPreparedStatement(Connection conn)
                                throws SQLException {
                            try {
                                return conn.prepareStatement(jsonBody.getCommandStr());
                            } catch (SQLException e) {
                                //deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage())));
                                throw new SQLException(e);
                            }

                            //return conn.prepareStatement(jsonBody.getCommandStr());

                        }
                    });
                    //jdbcTemplate.update(jsonBody.getCommandStr());
                    // System.out.println("id:" + keyHolder.getKey());
                    // return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(keyHolder.getKey())));
                }
//                if (!connection1.isClosed()) {
//                    try {
//                        connection1.close();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
                deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS)));//String.valueOf(keyHolder.getKey())));
            } catch (Exception e) {
                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
                deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage())));
            }
        }
        deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", "没有sql语句")));
    }
}

