package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.datasource.ShardingConfiguration;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.entity.SLDeferredResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Resource;

/**
 * @ClassName SlaveTaskManager
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/11
 **/
@Component
public class SlaveTaskManager {
    @Resource
    TransactionTemplate slavetxTemplate;
    @Resource
    JdbcTemplate slavejdbcTemplate;
    @Autowired
    ShardingConfiguration shardingConfiguration;

    //@Transactional(propagation = Propagation.REQUIRES_NEW,transactionManager = "capabilityDataSourceTransactionManager")
    public DeferredResult<String> exeTask(TMethodTypeEnum methodTypeEnum, JsonBody jsonBody) {
        // JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
        //System.out.println(jsonBody.getKeyNum());
        SLDeferredResult<String> deferredResult = new SLDeferredResult<String>(shardingConfiguration.getTransactionTimeout());
        Runnable runnable = getSlaveTask(methodTypeEnum, deferredResult, jsonBody);
        ;
        if (!(runnable == null)) {
            TaskRunnableFactory.es.execute(runnable);
        }
        //deferredResult.onTimeout(new TimeoutCallback(deferredResult));
        //deferredResult.onCompletion(new ResultCallback(runnable));
        return deferredResult;
    }

    public Runnable getSlaveTask(TMethodTypeEnum methodTypeEnum, SLDeferredResult<String> deferredResult, JsonBody jsonBody) {
        return TaskRunnableFactory.getRunnable(shardingConfiguration,slavejdbcTemplate, slavetxTemplate, methodTypeEnum, deferredResult, jsonBody);
    }
}
