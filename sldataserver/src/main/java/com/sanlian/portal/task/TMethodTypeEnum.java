package com.sanlian.portal.task;
/**
 * @ClassName TMethodTypeEnum
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/9
 * @Version V1.0
 **/
public enum TMethodTypeEnum {
    Select, Delete, Insert, DBTransaction, Update, Callable
}
