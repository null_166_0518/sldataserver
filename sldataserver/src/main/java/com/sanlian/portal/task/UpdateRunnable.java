package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.util.StringUtil;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.entity.DBColumnTypeEnum;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import org.apache.commons.codec.binary.Base64;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.web.context.request.async.DeferredResult;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @ClassName UpdateRunnable
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/9
 **/
public class UpdateRunnable implements Runnable {
    JsonBody jsonBody;
    JdbcTemplate jdbcTemplate;
    DeferredResult<String> deferredResult;

    public UpdateRunnable(JdbcTemplate jdbcTemplate, DeferredResult<String> deferredResult, JsonBody jsonBody) {
        this.jdbcTemplate = jdbcTemplate;
        this.deferredResult = deferredResult;
        this.jsonBody = jsonBody;

    }

    @Override
    public void run() {
        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
            try {
                String[] sqls = jsonBody.getCommandStr().split("\\#");
                for (String sql : sqls) {
                    JSONArray array = JSON.parseArray(jsonBody.getCommandBlob());
                    //int size = array.size();
                    if (array != null) {
                        jdbcTemplate.update(sql, new PreparedStatementSetter() {
                            @Override
                            public void setValues(PreparedStatement ps) throws SQLException {
                                int size = array.size() + 1;
                                for (int i = 1; i < size; i++) {
                                    JSONObject jso = (JSONObject) array.get(i - 1);
                                    try {
                                        //base64 = URLDecoder.decode(jso.get("BLOB").toString(), "utf-8");
                                        String base64 = jso.get(DBColumnTypeEnum.BLOB.name()).toString();
                                        if (StringUtil.isNotEmpty(base64)) {
                                            byte[] bs = new Base64().decode(base64);
                                            ps.setObject(i, bs);
                                        } else {
                                            ps.setObject(i, new byte[0]);
                                        }
                                    } catch (Exception e) {
                                        LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, jso.get("BLOB").toString() + e.getMessage(), e);
                                        ps.setObject(i, new byte[0]);
                                    }

                                }
                            }
                        });
                    } else {
                        int res = jdbcTemplate.update(sql);
                        System.out.println(res + " ========================");
                    }
                }
            } catch (Exception e) {
                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
                deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage())));
            }

        }
        deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS)));
    }
}

