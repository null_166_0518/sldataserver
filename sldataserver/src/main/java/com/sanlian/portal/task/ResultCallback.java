package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.sanlian.portal.datasource.DataSourceKey;
import com.sanlian.portal.datasource.DynamicDataSourceContextHolder;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.entity.RespBody;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * @ClassName ResultCallback
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/11
 **/
public class ResultCallback implements Runnable {
    SlaveTaskManager slaveTaskManager;
    TMethodTypeEnum methodTypeEnum;
    JsonBody jsonBody;
    DeferredResult<String> deferredResult;

    public ResultCallback(JsonBody jsonBody) {
        this.jsonBody = jsonBody;
    }

    public ResultCallback(SlaveTaskManager slaveTaskManager, TMethodTypeEnum methodTypeEnum, JsonBody jsonBody, DeferredResult<String> deferredResult) {
        this.slaveTaskManager = slaveTaskManager;
        this.methodTypeEnum = methodTypeEnum;
        this.jsonBody = jsonBody;
        this.deferredResult = deferredResult;
    }

    @Override
    public void run() {
        TaskRunnableFactory.watchRequests.remove(jsonBody.getKeyNum());
        if (slaveTaskManager != null) {
            RespBody respBody = JSON.parseObject((String) deferredResult.getResult(), RespBody.class);
            //System.out.println(respBody.getMessage());
            if (respBody.getCode().equals("1")) {
                slaveTaskManager.exeTask(methodTypeEnum, jsonBody);
            }
        }
    }
}
