package com.sanlian.portal.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.github.pagehelper.util.StringUtil;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.entity.DBColumnTypeEnum;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import oracle.jdbc.OracleTypes;
import org.apache.commons.codec.binary.Base64;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.request.async.DeferredResult;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @ClassName CallableRunnable
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/9
 **/
public class CallableRunnable implements Runnable {
    JsonBody jsonBody;
    JdbcTemplate jdbcTemplate;
    DeferredResult<String> deferredResult;

    public CallableRunnable(JdbcTemplate jdbcTemplate, DeferredResult<String> deferredResult,  JsonBody jsonBody) {
        this.jdbcTemplate = jdbcTemplate;
        this.deferredResult = deferredResult;
        this.jsonBody = jsonBody;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run() {
        //long time1 = System.currentTimeMillis();
        List<Map<String, Object>> list = new ArrayList<>();
        //JSONObject jsonObject = JSON.parseObject(param);
        //JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
            try {
                JSONArray intarray = JSON.parseArray(jsonBody.getCommandBlob());
                // 通过jdbcTemplate查询数据库
                list = (List<Map<String, Object>>) jdbcTemplate.execute(
                        new CallableStatementCreator() {
                            public CallableStatement createCallableStatement(Connection con) throws SQLException {
                                String storedProc = "{call " + jsonBody.getCommandStr() + "}";// 调用的sql
                                CallableStatement cs = con.prepareCall(storedProc);
                                if (intarray != null) {
                                    int size = intarray.size() + 1;
                                    for (int i = 1; i < size; i++) {
                                        JSONObject jso = (JSONObject) intarray.get(i - 1);
                                        Iterator iterator = jso.keySet().iterator();
                                        String key = (String) iterator.next();
                                        DBColumnTypeEnum dbColumnTypeEnum = DBColumnTypeEnum.valueOf(key.toUpperCase());
                                        switch (dbColumnTypeEnum) {
                                            case INT:
                                                cs.setInt(1, jso.getIntValue(key));// 设置输入参数的值
                                                break;
                                            case STRING:
                                                cs.setString(i, jso.getString(key));
                                                break;
                                            case BLOB:
                                                String base64 = jso.getString(key);
                                                if (StringUtil.isNotEmpty(base64)) {
                                                    byte[] bs = new Base64().decode(base64.replaceAll(" ", ""));
                                                    cs.setObject(i, bs);
                                                } else {
                                                    cs.setObject(i, new byte[0]);
                                                }
                                                break;
                                            case LONG:
                                                cs.setLong(i, jso.getLongValue(key));
                                                break;
                                            case FLOAT:
                                                cs.setFloat(i, jso.getFloatValue(key));
                                                break;
                                            case DOUBLE:
                                                cs.setDouble(i, jso.getDoubleValue(key));
                                                break;
                                            case DATE:
                                                cs.setTimestamp(i, jso.getTimestamp(key));
                                                break;
                                            case OUTCURSOR:
                                                cs.registerOutParameter(i, OracleTypes.CURSOR);
                                                break;
                                            case OUNUMBER:
                                                cs.registerOutParameter(i, OracleTypes.NUMBER);
                                                break;
                                            case OUTDOUBLE:
                                                cs.registerOutParameter(i, OracleTypes.DOUBLE);
                                                break;
                                            case OUTFLOAT:
                                                cs.registerOutParameter(i, OracleTypes.FLOAT);
                                                break;
                                            case OUTSTRING:
                                                cs.registerOutParameter(i, OracleTypes.VARCHAR);
                                                break;
                                        }
                                    }

                                }
                                return cs;
                            }
                        }, new CallableStatementCallback() {
                            public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
                                List resultsMap = new ArrayList();
                                cs.execute();
                                ResultSet rs = (ResultSet) cs.getObject(2);// 获取游标一行的值
                                while (rs.next()) {// 转换每行的返回值到Map中
                                    //System.out.println("------------------------");
                                    Map<String, Object> rowMap = new HashMap();
                                    int columnCount = rs.getMetaData().getColumnCount();
                                    for (int i = 1; i <= columnCount; i++) {
                                        // System.out.println(rs.getMetaData().getColumnName(i));
                                        rowMap.put(rs.getMetaData().getColumnName(i), rs.getObject(rs.getMetaData().getColumnName(i)));
//                                        if (rs.getMetaData().getColumnType(i)==Types.BLOB){
//                                            rowMap.put(rs.getMetaData().getColumnName(i), rs.getBytes(rs.getMetaData().getColumnName(i)));
//                                        }
                                    }
                                    resultsMap.add(rowMap);
                                }
                                rs.close();
                                return resultsMap;
                            }
                        });
            } catch (Exception e) {
                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
                deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage(), "")));
            }

        }
        if (list.size() > 0) {
            String result = JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(list.size()), list), new ValueFilter() {
                @Override
                public Object process(Object obj, String s, Object v) {
                    if (v == null)
                        return "";
                    return v;
                }
            });
            //long time2 = System.currentTimeMillis();
            //System.out.println((time2 - time1) + "=======" + reult);
            deferredResult.setResult(result);
        } else {
            deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("2", "0", "")));
        }

    }

}

