package com.sanlian.portal.httpclient;

import com.dtflys.forest.exceptions.ForestRuntimeException;
import com.dtflys.forest.http.ForestRequest;
import com.dtflys.forest.http.ForestResponse;
import com.dtflys.forest.interceptor.Interceptor;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName ErrorInterceptor
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/8/10
 **/
public class ErrorInterceptor implements Interceptor<Object> {
    @Override
    public void onError(ForestRuntimeException e, ForestRequest forestRequest, ForestResponse forestResponse) {
        LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, forestResponse.getContent(), e);
        Map<String, String> map = new HashMap<>();
        map.put("code", "0");
        map.put("message", forestResponse.getContent());
        forestResponse.setResult(map);
    }

    @Override
    public void onSuccess(Object stringStringMap, ForestRequest forestRequest, ForestResponse forestResponse) {
        Map<String, String> map = new HashMap<>();
        map.put("code", "1");
        map.put("message", forestResponse.getContent());
        //stringStringMap = map;
        forestResponse.setResult(map);
    }
}
