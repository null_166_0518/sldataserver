package com.sanlian.portal.httpclient;/**
 * Created by Soft001 on 2020/9/15.
 */

import com.dtflys.forest.annotation.DataVariable;
import com.dtflys.forest.annotation.Request;
import com.dtflys.forest.callback.OnError;
import com.dtflys.forest.callback.OnProgress;
import com.dtflys.forest.callback.OnSuccess;
import com.dtflys.forest.extensions.DownloadFile;
import com.dtflys.forest.http.ForestResponse;

import java.io.File;
import java.util.Map;

/**
 * @ClassName SLHttpClient
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/15
 * @Version V1.0
 **/
public interface SLHttpClient {
    @Request(
            url = "${url}",
            type = "post",
            data = "${content}",
            interceptor = ErrorInterceptor.class

    )
    Map<String, String> postJsonMap(@DataVariable("url") String url, @DataVariable("content") String content);

    @Request(
            url = "${url}",
            type = "post",
            data = "${content}"

    )
    ForestResponse<String> postJson(@DataVariable("url") String url, @DataVariable("content") String content);

    @Request(
            url = "${url}",
            type = "post",
            data = "${content}"
    )
    void postJsonCallBack(@DataVariable("url") String url, @DataVariable("content") String content, OnSuccess<String> onSuccess, OnError onError);

    @Request(
            url = "${url}",
            type = "get",
            data = "${content}"

    )
    ForestResponse<String> getJson(@DataVariable("url") String url, @DataVariable("content") String content);

    @Request(
            url = "${url}",
            type = "get",
            data = "${content}",
            interceptor = ErrorInterceptor.class
    )
    Map<String, String> getJsonMap(@DataVariable("url") String url, @DataVariable("content") String content);

    @Request(
            url = "${url}",
            type = "post",
            data = "${content}",
            async = true
    )
    void asyncPostJson(@DataVariable("url") String url, @DataVariable("content") String content, OnSuccess<String> onSuccess, OnError onError);

    @Request(
            url = "${url}",
            type = "post",
            data = "${content}",
            async = true
    )
    void asyncPostJson(@DataVariable("url") String url, @DataVariable("content") String content);

    @Request(
            url = "${url}",
            type = "get",
            data = "${content}",
            async = true,
            headers = {"Content-Type: application/json"}
    )
    void asyncGetJson(@DataVariable("url") String url, @DataVariable("content") String content, OnSuccess<String> onSuccess);

    @Request(url = "${url}")
    byte[] downloadFileToByteArray(@DataVariable("url") String url);

    @Request(url = "${url}")
    @DownloadFile(dir = "${dir}", filename = "${filename}")
    File downloadFile(@DataVariable("url") String url, @DataVariable("dir") String dir, @DataVariable("filename") String filename, OnProgress onProgress);

}
