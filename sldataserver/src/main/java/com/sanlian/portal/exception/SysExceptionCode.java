package com.sanlian.portal.exception;

public class SysExceptionCode {
    /*1类： 登录异常代码*/
    public static final String USER_IN_BLACKLIST = "用户在系统黑名单中";
    public static final String DEVICE_IN_BLACKLIST = "设备在系统黑名单中";
    public static final String INCORRECT_USERNAME_PASSWORD = "用户名密码不匹配";
    public static final String UNEXSIT_ACCOUNT = "账户不存在";
    public static final String INVALID_PASSWORD = "密码无效";
    public static final String ACCOUNT_NOT_ACTIVED = "账户未激活";
    public static final String NOT_ALLOWED_LOGIN_TIME = "不在规定时间内登录";
    public static final String NOT_ALLOWED_LOGIN_IP = "不在规定的ip内登录";
    public static final String PASSWORD_NEED_MODIFY = "密码需要修改";
    public static final String INVALID_ACTIVE_TIME = "账户激活已过期，请联系管理员";
    public static final String INVALID_PASSWORD_TIME = "密码已过期，请重新设置密码";
    //addyujian
    public static final String USERNAME_IN_SEESIONMAP = "用户已在别处登陆，账号多终端登陆异常";
    public static final String USERNAME_MAX_SEESION = "超出用户最大会话数";
    public static final String USERNAME_MAX_TIMEOUT = "用户长时间未操作，已失效";
    public static final String USERNAME_UserHighFrequencyNum = "用户高频访问，请稍后登陆";
    public static final String USERNAME_LOGIN_LONGTIME = "账号长时间未登陆已被禁用，请联系管理员激活";
    public static final String USERNAME_LOGIN_CHANGEPWD = "账户被激活，请修改密码";
    //add xiaxiang
    public static final String USERNAME_LOGIN_LXERROR = "用户类型不允许登录后台";
    public static final String RESOURCE_ERROR = "资源转换异常";

    /*2类： 系统异常代码*/
    public static final String INCORRECT_DATA_PARSE = "用户未激活，无法登录";
    public static final String IP_INCORRECT_DATA_PARSE = "用户访问ip段未设置，请激活用户设置有效ip段";
    public static final String TIME_INCORRECT_DATA_PARSE = "用户访问时间段未设置，请激活用户设置有效时间段";
    public static final String ERROR = "系统错误";
    public static final String FAILED = "操作失败";
    public static final String SUCCESS = "操作成功";
    public static final String USER_NO_PERMITION = "当前用户无此权限";
    public static final String PASSWORD_DECODE_ERROR = "密码解密错误";
    public static final String USER_NOT_AUTHEN = "用户未鉴权登录,请先鉴权登录";
    public static final String USER_AUTHEN_TIMEOUT = "用户鉴权已失效,请从鉴权登录";
    public static final String REDIS_ERROR="redis服务链接异常";
}