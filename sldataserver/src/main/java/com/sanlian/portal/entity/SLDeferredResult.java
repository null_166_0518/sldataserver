package com.sanlian.portal.entity;

import org.springframework.lang.Nullable;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.function.Supplier;

/**
 * @ClassName SLDeferredResult
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/15
 **/
public class SLDeferredResult<T> extends DeferredResult<T> {
    public SLDeferredResult(long currentTime) {
        this.currentTime = currentTime;
    }

    public SLDeferredResult(Long timeoutValue, long currentTime) {
        super(timeoutValue);
        this.currentTime = currentTime;
    }

    public SLDeferredResult(@Nullable Long timeoutValue, Object timeoutResult, long currentTime) {
        super(timeoutValue, timeoutResult);
        this.currentTime = currentTime;
    }

    public SLDeferredResult(@Nullable Long timeoutValue, Supplier timeoutResult, long currentTime) {
        super(timeoutValue, timeoutResult);
        this.currentTime = currentTime;
    }

    public SLDeferredResult(){

    }
    private long currentTime;

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }
}
