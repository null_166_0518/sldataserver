package com.sanlian.portal.entity;

/**
 * @ClassName DBColumnTypeEnum
 * @Description: 数据类型枚举
 * @Author soft001
 * @Date 2020/8/27
 **/
public enum DBColumnTypeEnum {
    FLOAT,DOUBLE,INT, STRING, BLOB,DATE,LONG,OUTCURSOR,OUNUMBER,OUTSTRING,OUTDOUBLE,OUTFLOAT
}
