package com.sanlian.portal.entity;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @ClassName JsonBody
 * @Description: 接口传参
 * @Author Soft001
 * @Date 2020/5/15
 **/
public class JsonBody implements Serializable {

    private static final long serialVersionUID = 1L;

    private String CommandStr = "";

    private String CommandBlob = "";

    private String keyNum = "";

    public void setKeyNum(String keyNum) {
        this.keyNum = keyNum;
    }

    public String getCommandStr() {
        return CommandStr;
    }

    public void setCommandStr(String commandStr) {
        CommandStr = commandStr;
    }

    public String getCommandBlob() {
        return CommandBlob;
    }

    public void setCommandBlob(String commandBlob) {
        CommandBlob = commandBlob;
    }


    @Override
    public String toString() {
        return "JsonBody{" +
                "CommandStr='" + CommandStr + '\'' +
//                ", CommandBlob='" + CommandBlob + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        int code = CommandStr.getBytes().length + CommandBlob.getBytes().length;
        return code;
    }

    public void setKeyNum() {
        this.keyNum =  StringUtils.replace(CommandStr, " ", "") + "_" + hashCode();;
    }

    public String getKeyNum() {
        return this.keyNum;
    }
}
