package com.sanlian.portal.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName TableRule
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/13
 **/
public class TableRule {
    private String actual_table_nodes;
    private String sharding_column;
    private String column_rule;
    private List<Integer> tablecount = new ArrayList<Integer>();

    public String getActual_table_nodes() {
        return actual_table_nodes;
    }

    public void setActual_table_nodes(String actual_table_nodes) {
        String nodes = actual_table_nodes;
        String[] points = nodes.split("\\..");
        int start = Integer.parseInt(points[0]);
        int end = Integer.parseInt(points[1]);
        int length = (end - start + 1);
        for (int j = start; j < length; j++) {
            tablecount.add(j);
        }

        this.actual_table_nodes = actual_table_nodes;
    }

    public String getSharding_column() {
        return sharding_column;
    }

    public void setSharding_column(String sharding_column) {
        this.sharding_column = sharding_column;
    }

    public String getColumn_rule() {
        return column_rule;
    }

    public void setColumn_rule(String column_rule) {
        this.column_rule = column_rule;
    }

    public List<Integer> getTablecount() {
        return tablecount;
    }

    public void setTablecount(List<Integer> tablecount) {
        this.tablecount = tablecount;
    }
}
