package com.sanlian.portal.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName RespBody
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/8/19
 **/
@ApiModel(value = "响应返回值", description = "用户接口的响应返回数据")
public class RespBody implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "返回码:0失败,1成功", required = true)
    private String code="1";
    @ApiModelProperty(value = "code为1时返回记录数,为0时返回错误信息", required = true)
    private String message="4";
    @ApiModelProperty(value = "返回结果集", required = true)
    private Object result="[{\"KSID\":\"99903\",\"LSH\":\"2200527879291\",\"KDDM\":\"362187\",\"JK\":\"\",\"KSRQ\":\"2020-08-07\",\"KSYYBM\":\"A\",\"CJ2\":\"\",\"KSYY\":\"初次申领\",\"CJ1\":\"合格\",\"DF1\":\"100\",\"JX\":\"赣州银园驾驶员培训有限公司\",\"DF2\":\"0\",\"KCH\":\"2\",\"KSSJ2\":\"\",\"XM\":\"彭博宇\",\"KSSJ1\":\"08:56:39\",\"YKRQ\":\"2020-08-07\",\"YYCS\":\"1\",\"KG\":\"叶青\",\"ZJHM\":\"362204199205295115\"}] ";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
