package com.sanlian.portal.datasource;

/**
 * Created by xianyu on 2019/8/28.
 */
public enum  DataSourceKey {
    master,
    slave
}
