package com.sanlian.portal.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * Created by xianyu on 2019/8/28.
 */
public class DynamicRoutingDataSource  extends AbstractRoutingDataSource{
    //private final Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    protected Object determineCurrentLookupKey() {
        //logger.info("Current DataSource is [{}]", DynamicDataSourceContextHolder.getDataSourceKey());
        return DynamicDataSourceContextHolder.getDataSource();
    }

}
