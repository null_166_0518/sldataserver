package com.sanlian.portal.datasource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by xianyu on 2019/8/28.
 */
public class DynamicDataSourceContextHolder {
    private static final Logger log = LoggerFactory.getLogger(DynamicDataSourceContextHolder.class);
    /**
     * 使用ThreadLocal把数据源与当前线程绑定
     */
    private static ThreadLocal<String> currentDataSource = new ThreadLocal<>();

    public static String getDataSource() {
        return currentDataSource.get();
    }

    public static void setDataSource(String dataSourceName) {
        //log.debug("Set dataSource: [" + dataSourceName + "]");
        //String current = currentDataSource.get();
       // if (!(MASTER_TRANSACTION.equals(current) || MASTER_ANNOTATION.equals(current))) {
            currentDataSource.set(dataSourceName);
       // }
        log.debug("====================查询从库======================");
        //log.debug("Current dataSource is: [" + currentDataSource.get() + "]");
    }

    public static void clearDataSource() {
        //log.debug("清除 Close dataSource: [" + currentDataSource.get() + "]");
        //log.debug("====================清除======================");
        currentDataSource.remove();
    }

}
