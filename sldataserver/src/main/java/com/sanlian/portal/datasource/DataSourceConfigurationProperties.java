package com.sanlian.portal.datasource;

import com.sanlian.portal.entity.TableRule;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName DataSourceConfigurationProperties
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/12
 **/
@ConfigurationProperties(prefix = "sharding", ignoreUnknownFields = true)
public class DataSourceConfigurationProperties {
    private String Masterip = "127.0.0.1";
    //实例名
    private String MasterServiceName = "SLEXAM";
    //用户名
    private String MasterUseranme = "ROADEXAM";
    //密码
    private String MasterPassword = "SLJTYY_888888";
    //IP
    private String Slaveip;
    //实例名
    private String SlaveServiceName;
    //用户名
    private String SlaveUseranme;
    //密码
    private String SlavePassword;
    //1.单例数据库，2.双机数据库
    private String SingleOrDouble = "1";
    //1.默认主库，2.切换从库
    private String SingletonMasterToSlave = "1";

    private String TransactionTimeout;

    private Map<String, TableRule> tables;

    public Map<String, TableRule> getTables() {
        return tables;
    }

    public void setTables(Map<String, TableRule> tables) {
        this.tables = tables;
    }

    public String getTransactionTimeout() {
        return TransactionTimeout;
    }

    public void setTransactionTimeout(String transactionTimeout) {
        TransactionTimeout = transactionTimeout;
    }

    public String getSingleOrDouble() {
        return SingleOrDouble;
    }

    public void setSingleOrDouble(String singleOrDouble) {
        SingleOrDouble = singleOrDouble;
    }

    public String getSingletonMasterToSlave() {
        return SingletonMasterToSlave;
    }

    public void setSingletonMasterToSlave(String singletonMasterToSlave) {
        SingletonMasterToSlave = singletonMasterToSlave;
    }

    public String getMasterip() {
        return Masterip;
    }

    public void setMasterip(String masterip) {
        Masterip = masterip;
    }

    public String getMasterServiceName() {
        return MasterServiceName;
    }

    public void setMasterServiceName(String masterServiceName) {
        MasterServiceName = masterServiceName;
    }

    public String getMasterUseranme() {
        return MasterUseranme;
    }

    public void setMasterUseranme(String masterUseranme) {
        MasterUseranme = masterUseranme;
    }

    public String getMasterPassword() {
        return MasterPassword;
    }

    public void setMasterPassword(String masterPassword) {
        MasterPassword = masterPassword;
    }

    public String getSlaveip() {
        return Slaveip;
    }

    public void setSlaveip(String slaveip) {
        Slaveip = slaveip;
    }

    public String getSlaveServiceName() {
        return SlaveServiceName;
    }

    public void setSlaveServiceName(String slaveServiceName) {
        SlaveServiceName = slaveServiceName;
    }

    public String getSlaveUseranme() {
        return SlaveUseranme;
    }

    public void setSlaveUseranme(String slaveUseranme) {
        SlaveUseranme = slaveUseranme;
    }

    public String getSlavePassword() {
        return SlavePassword;
    }

    public void setSlavePassword(String slavePassword) {
        SlavePassword = slavePassword;
    }
}
