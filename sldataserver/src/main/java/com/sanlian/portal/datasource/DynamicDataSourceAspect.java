package com.sanlian.portal.datasource;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by xianyu on 2019/8/28.
 */
//@Aspect
//@Component
public class DynamicDataSourceAspect {
    //private final String[] QUERY_PREFIX = {"select", "query", "get", "is"};
    //启用主从库：1：代表主库读写。2：代表从库读写。
    @Value("${MasterOrSlave}")
    private String MasterOrSlave="";

    //private static final Logger log = LoggerFactory.getLogger(DynamicDataSourceAspect.class);
    //@Pointcut("execution(* com.sljt.*.dao..*.select*(..)) ||execution(* com.sljt.*.dao.*.query*(..)) || execution(* com.sljt.*.dao.*.get*(..)) || execution(* com.sljt.*.dao..*.is*(..))")
    @Pointcut("execution( * com.sanlian.portal.controller.*.*(..))")
    //@Pointcut("execution(* com.sljt.*.service..*.select*(..)) ||execution(* com.sljt.*.service.*.query*(..)) || execution(* com.sljt.*.service.*.get*(..)) || execution(* com.sljt.*.service..*.is*(..))")
    public void findMethod() {
    }

    @Before("findMethod()")
    public void beforeFindMethod(JoinPoint point) {
        //System.out.println(point.getSignature().getName()+"================================"+point.getClass());
        if (MasterOrSlave.equals("2")) {
            DynamicDataSourceContextHolder.setDataSource(DataSourceKey.slave.name());
            // System.out.println(point.getSignature().getName()+"Find method: [begin], Current dataSource is: ["
            //         + DynamicDataSourceContextHolder.getDataSource() + "]");
        }

    }

    @After("findMethod()")
    public void afterFindMethod(JoinPoint point) {
        String dataSource = DynamicDataSourceContextHolder.getDataSource();
        //System.out.println(point.getSignature().getName()+"Find method: [end], Current dataSource is: [" + dataSource + "]");
        if (DataSourceKey.slave.name().equals(dataSource)) {
            DynamicDataSourceContextHolder.clearDataSource();
        }
    }

    //@Pointcut("execution( * com.sljt.km2.dao.*.*(..))||execution( * com.sljt.km3.dao.*.*(..))")
    /**
     @Pointcut("@annotation(com.sljt.system.annotation.BackUpAop)")
     //@Pointcut("execution(* com.sljt.*.service.*.add*(..)) || execution(* com.sljt.*.service.*.insert*(..)) || execution(* com.sljt.*.service.*.edit*(..))|| execution(* com.sljt.*.service.*.save*(..))|| execution(* com.sljt.*.service.*.delete*(..))|| execution(* com.sljt.*.service.*.update*(..))")
     public void editMethod() {
     }

     @Around("editMethod()") public void aroudMethod(ProceedingJoinPoint point) {
     Boolean isQueryMethod = isQueryMethod(point.getSignature().getName());
     if (!isQueryMethod) {
     if (Constans.MasterOrSlave == 1) {
     //插入主库
     if (InsertMaster(point)){
     //插入从库
     InsertSlave(point);
     }

     } else if (Constans.MasterOrSlave == 2) {
     InsertSlave(point);
     }

     }

     }

     /* @Before("editMethod()")
     public void beforeEditMethod(JoinPoint point) {
     DynamicDataSourceContextHolder.setDataSource(DataSourceKey.master.name());
     log.debug("Find method: [begin], Current dataSource is: ["
     + DynamicDataSourceContextHolder.getDataSource() + "]");
     }

     @After("editMethod()") public void afterEditMethod(JoinPoint point) {
     String dataSource = DynamicDataSourceContextHolder.getDataSource();
     log.debug("Find method: [end], Current dataSource is: [" + dataSource + "]");
     if (DataSourceKey.master.name().equals(dataSource)) {
     DynamicDataSourceContextHolder.clearDataSource();
     }
     }*/
    /**
     private Boolean isQueryMethod(String methodName) {
     for (String prefix : QUERY_PREFIX) {
     if (methodName.startsWith(prefix)) {
     return true;
     }
     }
     return false;
     }

     private boolean InsertMaster(ProceedingJoinPoint point) {
     try {
     //DynamicDataSourceContextHolder.setDataSource(DataSourceKey.master.name());
     //写入主库
     point.proceed();
     // String dataSource = DynamicDataSourceContextHolder.getDataSource();
     // if (DataSourceKey.master.name().equals(dataSource)) {
     //     DynamicDataSourceContextHolder.clearDataSource();
     // }
     } catch (Throwable throwable) {
     throwable.printStackTrace();
     return false;
     }
     return true;
     }

     private boolean InsertSlave(ProceedingJoinPoint point) {
     try {
     DynamicDataSourceContextHolder.setDataSource(DataSourceKey.slave.name());
     point.proceed();
     String dataSource = DynamicDataSourceContextHolder.getDataSource();
     if (DataSourceKey.slave.name().equals(dataSource)) {
     DynamicDataSourceContextHolder.clearDataSource();
     }
     } catch (Throwable throwable) {
     throwable.printStackTrace();
     return  false;
     }
     return  true;
     }
     **/
}
