package com.sanlian.portal.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dtflys.forest.http.ForestResponse;
import com.sanlian.filter.RequestFilter;
import com.sanlian.portal.httpclient.SLHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Map;

//import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;

/**
 * Created by xianyu on 2019/8/28.
 */
@Configuration
@EnableConfigurationProperties(DataSourceConfigurationProperties.class)
//@ConditionalOnProperty(prefix = "sharding", name = "rule-source")
public class DataSourceConfigurer {

    @Autowired
    private Environment environment;
    @Autowired
    private ConfigurableApplicationContext applicationContext;
    @Autowired
    private RequestFilter requestFilter;
    @Resource
    private SLHttpClient slHttpClient;

    @Bean
    //@Order(1)
    public ShardingConfiguration regristerShardingConfiguration(DataSourceConfigurationProperties dataSourceConfigurationProperties) {
        System.out.println("初始化配置");
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(ShardingConfiguration.class);
        String id = "shardingConfiguration";

        beanDefinitionBuilder
                .addPropertyValue("SingleOrDouble", dataSourceConfigurationProperties.getSingleOrDouble())
                .addPropertyValue("SingletonMasterToSlave", dataSourceConfigurationProperties.getSingletonMasterToSlave())
                .addPropertyValue("TransactionTimeout", dataSourceConfigurationProperties.getTransactionTimeout())
                .addPropertyValue("tables", dataSourceConfigurationProperties.getTables())
                .setLazyInit(false)
                .setFactoryMethod("configuration");

        // BeanDefinition interceptorFactoryBeanDefinition = registerInterceptorFactoryBean();
        // beanDefinitionBuilder.addPropertyValue("interceptorFactory", interceptorFactoryBeanDefinition);


        BeanDefinition beanDefinition = beanDefinitionBuilder.getRawBeanDefinition();
        //beanDefinition.getPropertyValues().addPropertyValue("sslKeyStores", sslKeystoreMap);

        BeanDefinitionRegistry beanFactory = (BeanDefinitionRegistry) applicationContext.getBeanFactory();
        beanFactory.registerBeanDefinition(id, beanDefinition);
        ShardingConfiguration configuration = applicationContext.getBean(id, ShardingConfiguration.class);
        return configuration;
    }

    /**
     * master DataSource
     *
     * @return data source
     * @Primary 注解用于标识默认使用的 DataSource Bean，因为有5个 DataSource Bean，该注解可用于 master
     * 或 slave DataSource Bean, 但不能用于 dynamicDataSource Bean, 否则会产生循环调用
     * @ConfigurationProperties 注解用于从 application.properties 文件中读取配置，为 Bean 设置属性
     */
    @Bean(name = "master")
    //@Order(2)
    //@Primary
    //@ConfigurationProperties(prefix = "spring.datasource.druid.master")
    public DataSource master(DataSourceConfigurationProperties dataSourceConfigurationProperties) {
        return createDruidDataSource(dataSourceConfigurationProperties);
    }

    /**
     * Slave alpha data source.
     *
     * @return the data source
     */
    @Bean(name = "slave")
    //@Order(3)
    //@Primary
    //@ConfigurationProperties(prefix = "spring.datasource.druid.slave")
    public DataSource slave(DataSourceConfigurationProperties dataSourceConfigurationProperties) {
        // return DruidDataSourceBuilder.create().build();
        return createSlaveDruidDataSource(dataSourceConfigurationProperties);
    }

    // @Bean("dataSource")
    //@ConditionalOnBean(type = {"master","slave"})
//    public DynamicRoutingDataSource dynamicDataSource() {
//        DynamicRoutingDataSource dynamicRoutingDataSource = new DynamicRoutingDataSource();
//        Map<Object, Object> dataSourceMap = new HashMap<>(2);
//        DataSource master = master();
//        DataSource slave = slave();
//        dataSourceMap.put(DataSourceKey.master.name(), master);
//        dataSourceMap.put(DataSourceKey.slave.name(), slave);
//
//        // 将 master 数据源作为默认指定的数据源
//        dynamicRoutingDataSource.setDefaultTargetDataSource(slave);
//        // 将 master 和 slave 数据源作为指定的数据源
//        dynamicRoutingDataSource.setTargetDataSources(dataSourceMap);
//
//        return dynamicRoutingDataSource;
//    }


    /**
     * 注入 DataSourceTransactionManager 用于事务管理
     */
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(DataSource master) {
//        return new DataSourceTransactionManager(dynamicDataSource());
        return new DataSourceTransactionManager(master);
    }

    /**
     * 注入 DataSourceTransactionManager 用于事务管理
     */
    @Bean(name = "slavetransactionManager")
    public PlatformTransactionManager slavetransactionManager(DataSource slave) {
//        return new DataSourceTransactionManager(dynamicDataSource());
        return new DataSourceTransactionManager(slave);
    }

    /**
     * JDBC事务操作配置
     */
    @Bean(name = "txTemplate")
    public TransactionTemplate transactionTemplate(@Qualifier("transactionManager") PlatformTransactionManager transactionManager) {
        return new TransactionTemplate(transactionManager);
    }

    /**
     * JDBC事务操作配置
     */
    @Bean(name = "slavetxTemplate")
    public TransactionTemplate slavetransactionTemplate(@Qualifier("slavetransactionManager") PlatformTransactionManager slavetransactionManager) {
        return new TransactionTemplate(slavetransactionManager);
    }

    /**
     * JDBC操作配置
     */
    @Bean(name = "jdbcTemplate")
    public JdbcTemplate jdbcTemplate(DataSource master) {
//        return new JdbcTemplate(dynamicDataSource());
        return new JdbcTemplate(master);
    }

    /**
     * JDBC操作配置
     */
    @Bean(name = "slavejdbcTemplate")
    public JdbcTemplate slavejdbcTemplate(DataSource slave) {
//        return new JdbcTemplate(dynamicDataSource());
        return new JdbcTemplate(slave);
    }

    /**
     * 注册ServletRegistrationBean
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean registrationBean() {
        ServletRegistrationBean bean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        /** 初始化参数配置，initParams**/
        //白名单
        bean.addInitParameter("allow", "127.0.0.1");//多个ip逗号隔开
        //IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的话提示:Sorry, you are not permitted to view this page.
        //bean.addInitParameter("deny", "192.168.1.110");
        //登录查看信息的账号密码.
        bean.addInitParameter("loginUsername", "admin");
        bean.addInitParameter("loginPassword", "admin");
        //是否能够重置数据.
        bean.addInitParameter("resetEnable", "false");
        return bean;
    }

    /**
     * 注册FilterRegistrationBean
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean druidStatFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean(new WebStatFilter());
        //添加过滤规则.
        bean.addUrlPatterns("/*");
        //添加不需要忽略的格式信息.
        bean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        return bean;
    }

    private DruidDataSource createDruidDataSource(DataSourceConfigurationProperties dataSourceConfigurationProperties) {
        System.out.println("初始化主库");
        DruidDataSource datasource = new DruidDataSource();
        // datasource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        datasource.setDriverClassName("oracle.jdbc.OracleDriver");
//        String result = HttpUtils.doPost("http://" + environment.getProperty("GLURL") + ":9601/SL04/DBCommand/DBConnection", "ED816AC533CD82F5566A3566B56253E2");
        try {
            reqDataBaseFromCenter(datasource, "ED816AC533CD82F5566A3566B56253E2");
        } catch (Exception e) {
            System.out.println("无法获取数据库连接信息，请求管理平台失败 " + "http://" + environment.getProperty("GLURL") + ":9601/SL04/DBCommand/DBConnection");
//            datasource.setUrl("jdbc:oracle:thin:@" + environment.getProperty("Masterip") + ":1521:" + environment.getProperty("MasterServiceName"));
//            datasource.setUsername(environment.getProperty("MasterUseranme"));
//            datasource.setPassword(environment.getProperty("MasterPassword"));
            datasource.setUrl("jdbc:oracle:thin:@" + dataSourceConfigurationProperties.getMasterip() + ":1521:" + dataSourceConfigurationProperties.getMasterServiceName());
            datasource.setUsername(dataSourceConfigurationProperties.getMasterUseranme());
            datasource.setPassword(dataSourceConfigurationProperties.getMasterPassword());
        }
//        if (response.isError()) {
//            System.out.println("无法获取数据库连接信息，请求管理平台失败 " + "http://" + environment.getProperty("GLURL") + ":9601/SL04/DBCommand/DBConnection");
////            datasource.setUrl("jdbc:oracle:thin:@" + environment.getProperty("Masterip") + ":1521:" + environment.getProperty("MasterServiceName"));
////            datasource.setUsername(environment.getProperty("MasterUseranme"));
////            datasource.setPassword(environment.getProperty("MasterPassword"));
//            datasource.setUrl("jdbc:oracle:thin:@" + dataSourceConfigurationProperties.getMasterip() + ":1521:" + dataSourceConfigurationProperties.getMasterServiceName());
//            datasource.setUsername(dataSourceConfigurationProperties.getMasterUseranme());
//            datasource.setPassword(dataSourceConfigurationProperties.getMasterPassword());
//        } else {
//            String result = response.getResult();
//            JSONObject map = JSON.parseObject(result);
//            datasource.setUrl("jdbc:oracle:thin:@" + map.get("DBIP").toString() + ":1521:" + map.get("ServiceName").toString());
//            datasource.setUsername(map.get("ServiceName").toString());
//            datasource.setPassword(map.get("ServiceName").toString());
//        }
        //configuration
        datasource.setInitialSize(5);
        datasource.setMinIdle(5);
        datasource.setMaxActive(50);
        datasource.setMaxWait(60000);
        datasource.setTimeBetweenEvictionRunsMillis(60000);
        datasource.setMinEvictableIdleTimeMillis(300000);
        //datasource.setValidationQuery("select 'x' FROM DUAL");
        datasource.setTestWhileIdle(false);
        datasource.setTestOnBorrow(false);
        datasource.setTestOnReturn(false);
        datasource.setPoolPreparedStatements(true);
        datasource.setMaxPoolPreparedStatementPerConnectionSize(50);
        datasource.setConnectionProperties("druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000");
        datasource.setDbType("com.alibaba.druid.pool.DruidDataSource");
        return datasource;
    }

    private DruidDataSource createSlaveDruidDataSource(DataSourceConfigurationProperties dataSourceConfigurationProperties) {
        System.out.println("初始化从库");
        DruidDataSource datasource = new DruidDataSource();
        //datasource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        datasource.setDriverClassName("oracle.jdbc.OracleDriver");
        // String result = HttpUtils.doPost("http://" + environment.getProperty("GLURL") + ":9601/SL04/DBCommand/DBConnection", "ED816AC533CD82F5566A3566B56253E2");
        try {
            reqDataBaseFromCenter(datasource, "ED816AC533CD82F5566A3566B56253E2");
        } catch (Exception e) {
            System.out.println("无法获取数据库连接信息，请求管理平台失败 " + "http://" + environment.getProperty("GLURL") + ":9601/SL04/DBCommand/DBConnection");
//            datasource.setUrl("jdbc:oracle:thin:@" + environment.getProperty("Slaveip") + ":1521:" + environment.getProperty("SlaveServiceName"));
//            datasource.setUsername(environment.getProperty("SlaveUseranme"));
//            datasource.setPassword(environment.getProperty("SlavePassword"));
            datasource.setUrl("jdbc:oracle:thin:@" + dataSourceConfigurationProperties.getSlaveip() + ":1521:" + dataSourceConfigurationProperties.getSlaveServiceName());
            datasource.setUsername(dataSourceConfigurationProperties.getSlaveUseranme());
            datasource.setPassword(dataSourceConfigurationProperties.getSlavePassword());
        }

        //configuration
        datasource.setInitialSize(5);
        datasource.setMinIdle(5);
        datasource.setMaxActive(50);
        datasource.setMaxWait(60000);
        datasource.setTimeBetweenEvictionRunsMillis(60000);
        datasource.setMinEvictableIdleTimeMillis(300000);
        //datasource.setValidationQuery("select 'x' FROM DUAL");
        datasource.setTestWhileIdle(false);
        datasource.setTestOnBorrow(false);
        datasource.setTestOnReturn(false);
        datasource.setPoolPreparedStatements(true);
        datasource.setMaxPoolPreparedStatementPerConnectionSize(50);
        datasource.setConnectionProperties("druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000");
        datasource.setDbType("com.alibaba.druid.pool.DruidDataSource");
        return datasource;
    }

//    /**
//     * shardingjdbc
//     * @return
//     */
//    //@Bean
//    DataSource getMasterSlaveDataSource() {
//        MasterSlaveRuleConfiguration masterSlaveRuleConfig = new MasterSlaveRuleConfiguration("ds_e", "ds_master", Arrays.asList("ds_slave"));
//        try {
//            return MasterSlaveDataSourceFactory.createDataSource(createDataSourceMap(), masterSlaveRuleConfig, new Properties());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return createDruidDataSource();
//    }
//
//    Map<String, DataSource> createDataSourceMap() {
//        Map<String, DataSource> result = new HashMap<>();
//        result.put("ds_master", createDruidDataSource());
//        result.put("ds_slave", createSlaveDruidDataSource());
//        return result;
//    }
//
//    /**
//     * 配置读写分离数据源
//     * @return
//     * @throws FileNotFoundException
//     * @throws SQLException
//     * @throws IOException
//     */
//    //@Bean
//    public DataSource dataSource() {
//        try {
//            return YamlMasterSlaveDataSourceFactory.createDataSource(ResourceUtils.getFile("classpath:sharding-jdbc.yml"));
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    private void reqDataBaseFromCenter(DruidDataSource datasource, String param) {
        ForestResponse<String> response = slHttpClient.postJson("http://" + environment.getProperty("GLURL") + ":9601/SL04/DBCommand/DBConnection", param);
        JSONObject map = JSON.parseObject(response.getResult());
        datasource.setUrl("jdbc:oracle:thin:@" + map.get("DBIP").toString() + ":1521:" + map.get("ServiceName").toString());
        datasource.setUsername(map.get("ServiceName").toString());
        datasource.setPassword(map.get("ServiceName").toString());
    }

    @Bean
    public FilterRegistrationBean registerAuthFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(requestFilter);
        registration.addUrlPatterns("/SL04/DBCommand/*");
        registration.setName("requestFilter");
        registration.setOrder(1);  //值越小，Filter越靠前。
        return registration;
    }
}
