package com.sanlian.portal.datasource;

import com.sanlian.portal.entity.TableRule;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName ShardingConfiguration
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/12
 **/
public class ShardingConfiguration implements Serializable {
    //1.单例数据库，2.双机数据库
    private String SingleOrDouble;
    //1.默认主库，2.切换从库
    private String SingletonMasterToSlave;
    //事务请求超时时间默认10秒，单位毫秒
    private Long TransactionTimeout;

    private Map<String, TableRule> tables;

    public Map<String,TableRule> getTables() {
        return tables;
    }

    public void setTables(Map<String,TableRule> tables) {
        this.tables = tables;
    }

    public Long getTransactionTimeout() {
        return TransactionTimeout;
    }

    public void setTransactionTimeout(Long transactionTimeout) {
        TransactionTimeout = transactionTimeout;
    }

    public String getSingleOrDouble() {
        return SingleOrDouble;
    }

    public void setSingleOrDouble(String singleOrDouble) {
        SingleOrDouble = singleOrDouble;
    }

    public String getSingletonMasterToSlave() {
        return SingletonMasterToSlave;
    }

    public void setSingletonMasterToSlave(String singletonMasterToSlave) {
        SingletonMasterToSlave = singletonMasterToSlave;
    }

    public static ShardingConfiguration configuration() {
        ShardingConfiguration shardingConfiguration = new ShardingConfiguration();
        shardingConfiguration.setSingleOrDouble("1");
        shardingConfiguration.setSingletonMasterToSlave("1");
        shardingConfiguration.setTransactionTimeout(10000L);
        return shardingConfiguration;
    }
}
