package com.sanlian.portal.interceptor;

import com.alibaba.fastjson.JSON;
import com.sanlian.portal.config.Common;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.datasource.ShardingConfiguration;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.entity.TableRule;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import com.sanlian.portal.task.TMethodTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @ClassName TableRuleInterceptor
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/12
 **/
@Component
@Aspect
public class TableRuleInterceptor {
    @Autowired
    ShardingConfiguration shardingConfiguration;

    @Pointcut("@annotation(com.sanlian.portal.annotation.TableRuleIntercept)")
    public void execMethod() {

    }

    @Around("execMethod()")
    public Object invoke(ProceedingJoinPoint invocation) throws Throwable {
        Object[] args = invocation.getArgs();
        if (shardingConfiguration.getTables() != null) {
            //int size = shardingConfiguration.getTables().size();
            if (StringUtils.isNotEmpty((String) args[0])) {
                JsonBody jsonBody = JSON.parseObject((String) args[0], JsonBody.class);
                String sql = jsonBody.getCommandStr().toUpperCase();
                TMethodTypeEnum method = TMethodTypeEnum.valueOf(invocation.getSignature().getName());
                Map<String, TableRule> tables = shardingConfiguration.getTables();
                for (Map.Entry<String, TableRule> enty : tables.entrySet()) {
                    String mapKey = enty.getKey();
                    if (!StringUtils.contains(sql, mapKey)) {
                        continue;
                    }
                    TableRule mapValue = enty.getValue();
                    // shardingConfiguration.getTables().forEach((mapKey, mapValue) -> {
                    mapKey = mapKey.toUpperCase();
                    StringBuffer newsql = new StringBuffer(100);

                    int eq_index = -1;
                    int less_index = -1;
                    int lesseq_index = -1;
                    int greater_index = -1;
                    int greatereq_index = -1;
                    int size = mapValue.getTablecount().size();
                    int start = mapValue.getTablecount().get(0);
                    int end = mapValue.getTablecount().get(size - 1);
                    switch (method) {
                        case Insert:
                            int months = Integer.parseInt(Common.fromDateM(new Date()));
                            //按月份对分表数目去模
                            int mod = Math.floorMod(months, mapValue.getTablecount().size());
                            String tableName = mapKey + "_" + mod;
                            //jsonBody.setCommandStr(StringUtils.replace(sql, mapKey, tableName));
                            newsql.append(StringUtils.replaceIgnoreCase(jsonBody.getCommandStr(), mapKey, tableName));
                            break;
                        case Select:
                            String deleteWhitespaceSql = StringUtils.deleteWhitespace(sql);
                            String upperColumn = mapValue.getSharding_column().toUpperCase();
                            String eq = upperColumn + "=";
                            String less = upperColumn + "<";
                            String lesseq = upperColumn + "<=";
                            String greater = upperColumn + ">";
                            String greatereq = upperColumn + ">=";
                            eq_index = StringUtils.indexOf(deleteWhitespaceSql, eq);
                            less_index = StringUtils.indexOf(deleteWhitespaceSql, less);
                            lesseq_index = StringUtils.indexOf(deleteWhitespaceSql, lesseq);
                            greater_index = StringUtils.indexOf(deleteWhitespaceSql, greater);
                            greatereq_index = StringUtils.indexOf(deleteWhitespaceSql, greatereq);
                            if (eq_index != -1) {
                                start = eq_index + eq.length() + 1;
                                end = start + mapValue.getColumn_rule().length();
                                int month = Integer.parseInt(Common.fromDateM(new SimpleDateFormat(mapValue.getColumn_rule()).parse(StringUtils.substring(deleteWhitespaceSql, start, end))));
                                mod = Math.floorMod(month, size);
                                tableName = mapKey + "_" + mod;
                                newsql.append(StringUtils.replaceIgnoreCase(jsonBody.getCommandStr(), mapKey, tableName));
                            } else if ((less_index == -1) && (lesseq_index == -1) && (greater_index == -1) && (greatereq_index == -1)) {
                                for (int i = start; i <= end; i++) {
                                    unionSelectAll(i, end, mapKey, newsql, jsonBody.getCommandStr(), i);
                                }
                            } else {
                                if (less_index != -1 || greater_index != -1) {
                                    checkLessOrGreater(newsql, mapKey, deleteWhitespaceSql, less_index, greater_index, mapValue, jsonBody.getCommandStr(),less.length(),greater.length());
                                } else if (less_index != -1 || greatereq_index != -1) {
                                    checkLessOrGreater(newsql, mapKey, deleteWhitespaceSql, less_index, greatereq_index, mapValue, jsonBody.getCommandStr(),less.length(),greatereq.length());
                                } else if (lesseq_index != -1 || greatereq_index != -1) {
                                    checkLessOrGreater(newsql, mapKey, deleteWhitespaceSql, lesseq_index, greatereq_index, mapValue, jsonBody.getCommandStr(),lesseq.length(),greatereq.length());
                                } else if (lesseq_index != -1 || greater_index != -1) {
                                    checkLessOrGreater(newsql, mapKey, deleteWhitespaceSql, lesseq_index, greater_index, mapValue, jsonBody.getCommandStr(),lesseq.length(),greater.length());
                                }
                            }
                            break;
                        case Update:

                            unionDeleteOrupdateAll(start, end, mapKey, newsql, jsonBody.getCommandStr());

                            break;
                        case Delete:

                            unionDeleteOrupdateAll(start, end, mapKey, newsql, jsonBody.getCommandStr());

                            break;
                        default:
                            newsql.append(jsonBody.getCommandStr());
                            break;

                    }
                    jsonBody.setCommandStr(newsql.toString());
                    args[0] = JSON.toJSONString(jsonBody);
                    // });
                }
            }
        }
        return invocation.proceed(args);
    }

    private void checkLessOrGreater(StringBuffer newsql, String mapKey, String deleteWhitespaceSql, int less_index, int greater_index, TableRule tableRule, String sql, int lindexLength,int rindexLength) {
        try {
            int lstart = less_index + lindexLength + 1;
            int rstart = greater_index + rindexLength + 1;
            int lend = lstart + tableRule.getColumn_rule().length();
            int rend = rstart + tableRule.getColumn_rule().length();
            String startyear = greater_index == -1 ? Common.fromDateY() : Common.fromDateY(new SimpleDateFormat(tableRule.getColumn_rule()).parse(StringUtils.substring(deleteWhitespaceSql, lstart, lend)));
            String endyear = greater_index == -1 ? Common.fromDateY() : Common.fromDateY(new SimpleDateFormat(tableRule.getColumn_rule()).parse(StringUtils.substring(deleteWhitespaceSql, rstart, rend)));
            int lessmonth = less_index == -1 ? -1 : Integer.parseInt(Common.fromDateM(new SimpleDateFormat(tableRule.getColumn_rule()).parse(StringUtils.substring(deleteWhitespaceSql, lstart, lend))));
            int greatermonth = greater_index == -1 ? -1 : Integer.parseInt(Common.fromDateM(new SimpleDateFormat(tableRule.getColumn_rule()).parse(StringUtils.substring(deleteWhitespaceSql, rstart, rend))));
            int size = tableRule.getTablecount().size();
            if (startyear.equals(endyear)) {
                if (lessmonth != greatermonth) {
                    if (lessmonth == -1) {
                        lessmonth = 1;
                    } else if (greatermonth == -1) {
                        greatermonth = Integer.parseInt(Common.fromDateM(new Date()));
                    }
                    for (int j = lessmonth; j <= greatermonth; j++) {
                        int mod = Math.floorMod(j, size);
                        unionSelectAll(j, greatermonth, mapKey, newsql, sql, mod);
                    }
                } else {
                    String Name = mapKey + "_" + Math.floorMod(lessmonth, size);
                    newsql.append(StringUtils.replaceIgnoreCase(sql, mapKey, Name));
                }
            } else {
                int start = tableRule.getTablecount().get(0);
                int end = tableRule.getTablecount().get(size - 1);
                for (int i = start; i <= end; i++) {
                    unionSelectAll(i, end, mapKey, newsql, sql, i);
                }
            }

        } catch (Exception e) {
            LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, "接口出现异常:" + e.getMessage(), e);
        }
    }

    private void unionSelectAll(int i, int end, String mapKey, StringBuffer newsql, String sql, int mod) {
        String Name = mapKey + "_" + mod;
        if (i == end) {
            newsql.append(StringUtils.replaceIgnoreCase(sql, mapKey, Name));
        } else {
            newsql.append(StringUtils.replaceIgnoreCase(sql, mapKey, Name)).append(" union ");
        }
    }

    private void unionDeleteOrupdateAll(int start, int end, String mapKey, StringBuffer newsql, String sql) {
        for (int i = start; i <= end; i++) {
            String Name = mapKey + "_" + i;
            if (i == end) {
                newsql.append(StringUtils.replaceIgnoreCase(sql, mapKey, Name));
            } else {
                newsql.append(StringUtils.replaceIgnoreCase(sql, mapKey, Name)).append("#");
            }
        }
    }
}
