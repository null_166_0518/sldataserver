package com.sanlian.portal.log.factory;

import com.sanlian.portal.config.Common;
import com.sanlian.portal.log.Log;
import com.sanlian.portal.log.LogType;

import java.util.Date;

/*
 * 
 * @描述:TODO (log实体工厂类产生Log)
 * @ClassName: LogFactory
 * @author yujian 
 * @date 2019年4月12日
 */
public class LogFactory {

    private LogFactory() {

    }

    /**
     * 
     * @描述:
     * @Title: getLog
     * @param code
     * @param message
     * @return Log
     * @日期 2019年6月28日
     */
    public static Log getLog(LogType logType, String code, String message) {
        Log log = new Log();
        log.setLogType(logType);
        log.setCode(code);
        log.setMessage(message);
        log.setActionTime(Common.format.format(new Date()));
        return log;
    }

}
