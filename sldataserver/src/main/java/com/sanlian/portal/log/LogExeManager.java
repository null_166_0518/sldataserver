package com.sanlian.portal.log;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.log.factory.LogTaskFactory;

import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/*
 * 
 * @描述:TODO (日志操作任务运行管理器 单例)
 * @ClassName: LogExeManager
 * @author 余舰 
 * @date 2019年6月24日
 */
public class LogExeManager {

    private static final int LOG_DELAY_TIME = 10;

    private ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("log-pool-%d").build();
    private ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(20, threadFactory);

    private static LogExeManager logExeManager = new LogExeManager();

    private LogExeManager() {

    }

    public static LogExeManager getInstance() {
        return logExeManager;
    }

    public void executeLogTask(LogType logType, String code, String message, Exception e) {
        if (e != null) {
           // e.printStackTrace();
        }
        TimerTask timerTask = LogTaskFactory.excuteLog(logType, code, message);
        executor.schedule(timerTask, LOG_DELAY_TIME, TimeUnit.MICROSECONDS);
    }
}
