package com.sanlian.portal.log.factory;

import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sanlian.portal.config.Constans;
import com.sanlian.portal.log.Log;
import com.sanlian.portal.log.LogType;

/*
 * 
 * @描述:TODO (日志任务定时器，执行写入日志信息)
 * @ClassName: LogTaskFactory
 * @author 余舰 
 * @date 2019年4月17日
 */
public class LogTaskFactory {

    private static Logger LOGGER = LoggerFactory.getLogger(LogTaskFactory.class);

    private LogTaskFactory() {

    }

    public static TimerTask excuteLog(LogType logType, String code, String message) {
        Log log = LogFactory.getLog(logType, code, message);
        return new TimerTask() {
            @Override
            public void run() {
                try {
                    //System.out.println("开始执行日志");
                    LogType type = log.getLogType();
                    switch (type) {
                    case TRACE:
                        LOGGER.trace(log.toString());
                        break;
                    case INFO:
                        LOGGER.info(log.toString());
                        break;
                    default:
                        LOGGER.error(log.toString());
                        break;
                    }
                    // logService.addCacheListWithTime(log.getActionTime(), log,
                    // 7, TimeUnit.DAYS);
                    //System.out.println("结束执行日志");
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error("写入日志异常", e.getCause().getMessage());
                }
            }
        };
    }

}
