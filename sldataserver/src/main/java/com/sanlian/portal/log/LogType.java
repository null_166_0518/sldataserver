package com.sanlian.portal.log;

public enum LogType {
    TRACE, INFO, ERROR;
}
