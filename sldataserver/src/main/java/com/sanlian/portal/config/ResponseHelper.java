package com.sanlian.portal.config;

import com.github.pagehelper.Page;
import com.sanlian.portal.entity.RespBody;
import org.springframework.http.HttpStatus;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ResponseHelper {

    public ResponseHelper() {
    }

    public static <T> ResponseModel<T> notFound(String message) {
        ResponseModel response = new ResponseModel();
        response.setStatus(HttpStatus.NOT_FOUND.value());
        response.setCode(HttpStatus.NOT_FOUND.getReasonPhrase());
        response.setMessage(message);
        return response;
    }

    public static <T> ResponseModel<T> internalServerError(String message) {
        ResponseModel response = new ResponseModel();
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        response.setMessage(message);
        return response;
    }

    public static <T> ResponseModel<T> notAllowedError(String message) {
        ResponseModel response = new ResponseModel();
        response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
        response.setCode(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());
        response.setMessage(message);
        return response;
    }

    public static <T> ResponseModel<T> validationFailure(String message) {
        ResponseModel response = new ResponseModel();
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setCode(HttpStatus.BAD_REQUEST.getReasonPhrase());
        response.setMessage(message);
        return response;
    }

    public static <T> ResponseModel<T> buildResponseModel(T data) {
        ResponseModel response = new ResponseModel();
        response.setStatus(HttpStatus.OK.value());
        response.setCode(HttpStatus.OK.getReasonPhrase());
        response.setMessage(HttpStatus.OK.getReasonPhrase());
        response.setData(data);
        return response;
    }

    public static <T> ResponseModel<T> buildERRORResponseModel(T data) {
        ResponseModel response = new ResponseModel();
        response.setStatus(HttpStatus.GATEWAY_TIMEOUT.value());
        response.setCode(HttpStatus.GATEWAY_TIMEOUT.getReasonPhrase());
        response.setMessage(HttpStatus.OK.getReasonPhrase());
        response.setData(data);
        return response;
    }

    public static <T> ResponseModel<T> buildPageResponseModel(T data) {
        ResponseModel response = new ResponseModel();
        if (data instanceof Page) {
            Page page = (Page) data;
            response.setStatus(HttpStatus.OK.value());
            response.setCode(HttpStatus.OK.getReasonPhrase());
            response.setMessage(HttpStatus.OK.getReasonPhrase());
            response.setRecordsTotal(page.getTotal());
            response.setRecordsFiltered(page.getTotal());
            response.setData(page);
            return response;
        } else {
            throw new ClassCastException("数据类型异常");
        }
    }

    public static Map<String, String> buildResponseResult(String code, String message) {
        Map<String, String> response = new HashMap();
        response.put("code", code);
        try {
            response.put("message", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static RespBody buildResponseResult(String code, String message, Object result) {
        RespBody response=new RespBody();
        response.setCode(code);
        try {
            response.setMessage(message);//response.put("message", message);//URLEncoder.encode(message, "utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
       // response.put("result", result);
        response.setResult(result);
        return response;
    }
}
