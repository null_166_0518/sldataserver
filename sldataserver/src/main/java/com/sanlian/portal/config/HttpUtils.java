//package com.sanlian.portal.config;
//
//import com.github.pagehelper.util.StringUtil;
//import com.sanlian.portal.log.LogExeManager;
//import com.sanlian.portal.log.LogType;
//import org.apache.http.HttpEntity;
//import org.apache.http.client.config.RequestConfig;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
//
///**
// * @ClassName HttpUtils
// * @Description: TODO
// * @Author Soft001
// * @Date 2020/5/14
// **/
//public class HttpUtils {
//    /**
//     * 发送POST请求
//     *
//     * @param url  请求url
//     * @param data 请求数据
//     * @return 结果
//     */
//    @SuppressWarnings("deprecation")
//    public static String doPost(String url, String data) {
//        CloseableHttpClient httpClient = HttpClients.createDefault();
//        CloseableHttpResponse response = null;
//        String context = "";
//        HttpPost httpPost = new HttpPost(url);
//        try {
//            RequestConfig requestConfig = RequestConfig.custom()
//                    .setSocketTimeout(10000).setConnectTimeout(10000)
//                    .setConnectionRequestTimeout(10000).build();
//            httpPost.setConfig(requestConfig);
//            if (StringUtil.isNotEmpty(data)) {
//                StringEntity body = new StringEntity(data, "utf-8");
//                httpPost.setEntity(body);
//            }
//            // 设置回调接口接收的消息头
//            httpPost.addHeader("Content-Type", "application/json");
//            response = httpClient.execute(httpPost);
//            HttpEntity entity = response.getEntity();
//            context = EntityUtils.toString(entity, "utf-8");
//        } catch (Exception e) {
//            LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(),e);
//            //return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", "车载调用失败"));
//            return "0";
//        } finally {
//            try {
//                response.close();
//                httpPost.abort();
//                httpClient.close();
//            } catch (Exception e) {
//                e.getStackTrace();
//            }
//        }
//        return context;
//    }
//}
