package com.sanlian.portal.config;

import com.alibaba.fastjson.JSONObject;
import com.sanlian.portal.exception.SysExceptionCode;
import com.sanlian.portal.exception.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class AllControllerAdvice {
    private static Logger console = LoggerFactory.getLogger(AllControllerAdvice.class);

    /**
     * 全局异常捕捉处理
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public String errorHandler(Exception ex) {
        ex.printStackTrace();
        console.error("接口出现严重异常：{}", ex.getMessage());
        handler401(2, "系统出现严重异常：{}" + SysExceptionCode.ERROR);
        return JSONObject.toJSONString(ResponseHelper.validationFailure(SysExceptionCode.ERROR));
    }

    /**
     * 捕捉UnauthorizedException
     *
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public String handleUnauthorized() {
        return JSONObject.toJSONString(ResponseHelper.notAllowedError(SysExceptionCode.USER_NO_PERMITION));
    }


    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(InvalidKeySpecException.class)
    @ResponseBody
    public String handleInvalidKeySpecException(InvalidKeySpecException e) {
        return JSONObject.toJSONString(ResponseHelper.validationFailure(SysExceptionCode.PASSWORD_DECODE_ERROR));
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NoSuchAlgorithmException.class)
    @ResponseBody
    public String handleNoSuchAlgorithmException(NoSuchAlgorithmException e) {
        return JSONObject.toJSONString(ResponseHelper.validationFailure(SysExceptionCode.PASSWORD_DECODE_ERROR));
    }

    private void handler401(int code, String msg) {
        try {
            HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                    .getResponse();

            Map<String, Object> map = new HashMap<String, Object>();
            //List<String> messageList = new ArrayList<String>();
            //messageList.add(msg);
            map.put("message", msg);
            String res = JSONObject.toJSONString(ResponseHelper.buildERRORResponseModel(map));
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setStatus(HttpStatus.OK.value());
            response.setContentType("application/json;charset=utf-8");
            // httpResponse.getWriter().write("{\"code\":" + code + ", \"message\":\"" + msg + "\"}");
            //String res="{\"code\":" + code + ", \"message\":\"" + msg + "\"}";
            ServletOutputStream out = httpResponse.getOutputStream();
            out.write(res.getBytes("utf8"));
            out.flush();
            out.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}