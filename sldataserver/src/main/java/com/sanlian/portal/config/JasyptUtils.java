package com.sanlian.portal.config;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEByteEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;

/**
 * @Created with Intellij IDEA
 * @Author : payne
 * @Date : 2018/5/18 - 10:37
 * @Copyright (C), 2018-2018
 * @Descripition : Jasypt安全框架加密类工具包
 */
public class JasyptUtils {

    /**
     * Jasypt生成加密结果
     *
     * @param password 配置文件中设定的加密密码 jasypt.encryptor.password
     * @param value    待加密值
     * @return
     */
    public static String encryptPwd(String password, String value) {
        PooledPBEStringEncryptor encryptOr = new PooledPBEStringEncryptor();
        encryptOr.setConfig(cryptOr(password));
        String result = encryptOr.encrypt(value);
        return result;
    }

    /**
     * 解密
     *
     * @param password 配置文件中设定的加密密码 jasypt.encryptor.password
     * @param value    待解密密文
     * @return
     */
    public static String decyptPwd(String password, String value) {
        PooledPBEStringEncryptor encryptOr = new PooledPBEStringEncryptor();
        encryptOr.setConfig(cryptOr(password));
        String result = encryptOr.decrypt(value);
        return result;
    }

    public static SimpleStringPBEConfig cryptOr(String password) {
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword(password);
        config.setAlgorithm(StandardPBEByteEncryptor.DEFAULT_ALGORITHM);
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setStringOutputType("base64");
        return config;
    }

    public static void main(String[] args) {
        String url=encryptPwd("EbfYkitulv73I2p0mXI50JMXoaxZTKJ7", "jdbc:oracle:thin:@192.168.200.108:1521/SLEXAM");
        String username=encryptPwd("EbfYkitulv73I2p0mXI50JMXoaxZTKJ7", "SLPLAT");
        String pwd=encryptPwd("EbfYkitulv73I2p0mXI50JMXoaxZTKJ7", "SLJTYY_888888");
        // 加密
        System.out.println(url);
        System.out.println(username);
        System.out.println(pwd);

        // 解密
        System.out.println(decyptPwd("EbfYkitulv73I2p0mXI50JMXoaxZTKJ7", pwd));
    }

}
