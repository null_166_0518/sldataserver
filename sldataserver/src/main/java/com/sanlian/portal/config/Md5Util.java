package com.sanlian.portal.config;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 
 * @描述:TODO (md5校验码)
 * @ClassName: Md5Util
 * @author 吴波 
 * @date 2019年4月24日
 */
public class Md5Util {

	public static String getMd5Hex(String param){
		return DigestUtils.md5Hex(param);
	}

	public static void main(String[] args){
		System.out.println(getMd5Hex("user13401221989090267970Joker0"));
	}
}