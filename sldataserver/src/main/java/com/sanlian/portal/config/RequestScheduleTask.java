package com.sanlian.portal.config;

import com.sanlian.portal.datasource.ShardingConfiguration;
import com.sanlian.portal.entity.SLDeferredResult;
import com.sanlian.portal.task.TaskRunnableFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.Map;

/**
 * @ClassName RequestScheduleTask
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/15
 **/
@Component
public class RequestScheduleTask {
    @Resource
    ShardingConfiguration shardingConfiguration;
    @Scheduled(cron = "0/5 * * * * ?")
    public void deleteTimeoutRq(){
        for (Iterator iter = TaskRunnableFactory.watchRequests.entrySet().iterator(); iter.hasNext(); ) {
            Map.Entry<String,SLDeferredResult> entry= (Map.Entry<String, SLDeferredResult>) iter.next();
            SLDeferredResult slDeferredResult=entry.getValue();
            long time=slDeferredResult.getCurrentTime();
            long now=System.currentTimeMillis();
            if ((now-time)>=shardingConfiguration.getTransactionTimeout()){
                iter.remove();
            }
        }
    }
}
