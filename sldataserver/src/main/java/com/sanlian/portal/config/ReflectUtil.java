package com.sanlian.portal.config;

import java.lang.reflect.Field;

public class ReflectUtil {

	public static Object getReflectValue(Object object, String paramName) {
		Field field;
		try {
			field = object.getClass().getDeclaredField(paramName);
			field.setAccessible(true);
			Object result = field.get(object);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
