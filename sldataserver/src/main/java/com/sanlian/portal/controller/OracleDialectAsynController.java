package com.sanlian.portal.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.github.pagehelper.util.StringUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.sanlian.portal.annotation.TableRuleIntercept;
import com.sanlian.portal.config.Constans;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.entity.DBColumnTypeEnum;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.entity.RespBody;
import com.sanlian.portal.log.LogExeManager;
import com.sanlian.portal.log.LogType;
import com.sanlian.portal.task.TMethodTypeEnum;
import com.sanlian.portal.task.TransactionTaskManager;
import io.swagger.annotations.*;
import oracle.jdbc.OracleTypes;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.*;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Resource;
import java.sql.*;
import java.util.*;

/**
 * @ClassName OracleDialectController
 * @Description: 数据库操作接口控制层
 * @Author Soft001
 * @Date 2020/5/8
 **/
@RestController
@RequestMapping("/SL04/DBCommand/")
@Api(value = "/SL04/DBCommand/", description = "数据库接口控制类", tags = "接口管理")
public class OracleDialectAsynController {

    @Resource
    private TransactionTaskManager transactionTaskManager;

    /**
     * @MethodName:
     * @Description: TODO
     * @Param:
     * @Return:
     * @Author: Soft001
     * @Date: 2020/5/11
     **/
    @TableRuleIntercept
    @RequestMapping(value = "Select", method = RequestMethod.POST)
    @ApiOperation(value = "执行查询操作", notes = "返回查询结果信息", httpMethod = "POST")
    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
    public DeferredResult<String> Select(@RequestBody(required = false) @ApiParam(name = "json字符串,CommandStr:数据库执行语句", value = "{\"CommandStr\":\"select * from b_ks t where t.ykrq='2020-09-14'\"}", required = true) String param) {
        //long time1 = System.currentTimeMillis();

        return transactionTaskManager.exeTask(TMethodTypeEnum.Select, param);
    }

    /**
     * @MethodName:
     * @Description: TODO
     * @Param:
     * @Return:
     * @Author: Soft001
     * @Date: 2020/5/11
     **/
    @TableRuleIntercept
    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    @RequestMapping(value = "Delete", method = RequestMethod.POST)
    //@ApiImplicitParam(paramType = "Delete", name = "CommandStr", value = "sql语句", dataType = "string")
    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
    @ApiOperation(value = "执行删除操作", notes = "返回删除结果信息{“code”:“0”, “message”:“失败原因：xxx”}", httpMethod = "POST")
    public DeferredResult<String> Delete(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句", value = "{\"CommandStr\":\"delete table t where t.xx=1\"}", required = true) String param) {
        return transactionTaskManager.exeTask(TMethodTypeEnum.Delete, param);
    }


    /**
     * @MethodName:
     * @Description: TODO
     * @Param:
     * @Return:
     * @Author: Soft001
     * @Date: 2020/5/11
     **/
    @TableRuleIntercept
    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    @RequestMapping(value = "Insert", method = RequestMethod.POST)
    // @ApiImplicitParam(paramType = "Insert", name = "CommandStr", value = "sql语句", dataType = "string")
    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
    @ApiOperation(value = "执行新增操作", notes = "返回新增主键信息", httpMethod = "POST")
    public DeferredResult<String> Insert(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句,CommandBlob:BLOB类型JSON对象的键固定为“BLOB”,对应的值和语句中“?”的个数和顺序相互对应.值为base64", value = "{\"CommandStr\":\"insert into table(id,?,?) values('1',?,?)\",\"CommandBlo\":\"[{'BLOB':'ED816AC533CD82F5566A30'},{'BLOB':'ED816AC533CD82F5566A30'}]\"}", required = true) String param) {
        return transactionTaskManager.exeTask(TMethodTypeEnum.Insert, param);
    }


    /**
     * @MethodName:
     * @Description: TODO
     * @Param:
     * @Return:
     * @Author: Soft001
     * @Date: 2020/5/11
     **/
    @TableRuleIntercept
    @ApiOperation(value = "执行事务操作", notes = "返回事务操作结果信息", httpMethod = "POST")
    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
    @RequestMapping(value = "DBTransaction", method = RequestMethod.POST)
    public DeferredResult<String> DBTransaction(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句,CommandBlob:BLOB类型JSON对象的键固定为“BLOB”,对应的值和更新语句中“?”的个数和顺序相互对应.值为base64", value = "{'CommandStr':'update table set xx='刘德华',xx1=?,xx2=? where  xx3='2020-05-09;delete  from table  where xx='2020-05-09';insert into table(xx,xx) values ('1',?)','CommandBlob':'[[{'BLOB':'ED816AC533CD82F5566A30'},{'BLOB':'ED816AC533CD82F5566A30'}],[],[{'BLOB':'ED816AC533CD82F5566A30'}]]'}", required = true) String param) {
        return transactionTaskManager.exeTask(TMethodTypeEnum.DBTransaction, param);
    }


    /**
     * @MethodName:
     * @Description: 执行更改操作
     * @Param:
     * @Return:
     * @Author: yujian
     * @Date: 2020/5/11
     **/
    @TableRuleIntercept
   // @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RequestMapping(value = "Update", method = RequestMethod.POST)
    //@ApiImplicitParam(paramType = "Update", name = "CommandStr", value = "sql语句", dataType = "string")
    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
    @ApiOperation(value = "执行更新操作", notes = "返回更新结果信息", httpMethod = "POST")
    public DeferredResult<String> Update(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句,CommandBlob:BLOB类型JSON对象的键固定为“BLOB”,对应的值和语句中“?”的个数和顺序相互对应.值为base64", value = "{\"CommandStr\":\"update table t set t.xx=1,t.blob1=?,t.blob2=? where t.xx=1\",\"CommandBlob\":\"[{'BLOB':'ED816AC533CD82F5566A30'},{'BLOB':'ED816AC533CD82F5566A30'}]\"}", required = true) String param) {
        return transactionTaskManager.exeTask(TMethodTypeEnum.Update, param);
    }

     
     /**
      * @MethodName:
      * @Description: 执行存储过程
      * @Param: 
      * @Return: 
      * @Author: Soft001
      * @Date: 2020/9/13
     **/
    @TableRuleIntercept
    @ApiOperationSupport(author = "三联交通")
    @RequestMapping(value = "Callable", method = RequestMethod.POST)
    //@ApiImplicitParam(paramType = "body", name = "CommandStr", value = "sql语句", dataType = "string")
    @ApiOperation(value = "执行存储过程操作", notes = "返回存储过程信息", httpMethod = "POST")
    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
    public DeferredResult<String> Callable(@RequestBody @ApiParam(name = "CommandStr：存储过程名称入参出参用'?'代替，CommandBlob：入参Key包括（FLOAT,DOUBLE,INT, STRING, BLOB,DATE,LONG);出参key包括（OUTCURSOR,OUNUMBER,OUTSTRING,OUTDOUBLE,OUTFLOAT）,value为空串", value = "{'CommandStr':'p_getAllKSCJ(?,?,?)','CommandBlob':'[{'INT',1},{'STRING':'2'},{'OUTCURSOR':''}]'", required = true) String param) {
        return transactionTaskManager.exeTask(TMethodTypeEnum.Callable, param);
    }
}
