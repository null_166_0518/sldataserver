//package com.sanlian.portal.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.alibaba.fastjson.serializer.SerializerFeature;
//import com.alibaba.fastjson.serializer.ValueFilter;
//import com.github.pagehelper.util.StringUtil;
//import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
//import com.sanlian.portal.config.Constans;
//import com.sanlian.portal.config.ResponseHelper;
//import com.sanlian.portal.entity.DBColumnTypeEnum;
//import com.sanlian.portal.entity.JsonBody;
//import com.sanlian.portal.entity.RespBody;
//import com.sanlian.portal.log.LogExeManager;
//import com.sanlian.portal.log.LogType;
//import com.sanlian.portal.task.TMethodTypeEnum;
//import com.sanlian.portal.task.TransactionTaskManager;
//import io.swagger.annotations.*;
//import oracle.jdbc.OracleTypes;
//import org.apache.commons.codec.binary.Base64;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.DataAccessException;
//import org.springframework.jdbc.core.*;
//import org.springframework.transaction.TransactionStatus;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.support.TransactionCallback;
//import org.springframework.transaction.support.TransactionTemplate;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.context.request.async.DeferredResult;
//
//import java.sql.*;
//import java.util.*;
//
///**
// * @ClassName OracleDialectController
// * @Description: 数据库操作接口控制层
// * @Author Soft001
// * @Date 2020/5/8
// **/
//@RestController
//@RequestMapping("/SL04/DBCommand/")
//@Api(value = "/SL04/DBCommand/", description = "数据库接口控制类", tags = "接口管理")
//public class OracleDialectController {
//
//    @Autowired
//    TransactionTemplate txTemplate;
//    @Autowired
//    JdbcTemplate jdbcTemplate;
//    @Autowired
//    private TransactionTaskManager transactionTaskManager;
//
//    /**
//     * @MethodName:
//     * @Description: TODO
//     * @Param:
//     * @Return:
//     * @Author: Soft001
//     * @Date: 2020/5/11
//     **/
//    //@ResponseBody
//    @RequestMapping(value = "Select", method = RequestMethod.POST)
//    //@ApiImplicitParam(paramType = "[Select,json]",name = "[CommandStr,CommandBlob]", value = "sql语句", dataType = "string")
//    //@ApiImplicitParams({@ApiImplicitParam(paramType = "json",name = "CommandStr", value = "sql语句", dataType = "string"),@ApiImplicitParam(paramType = "json", name = "CommandBlob", value = "sql语句", dataType = "string")})
//    @ApiOperation(value = "执行查询操作", notes = "返回查询结果信息", httpMethod = "POST")
//    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
//    public String exeSelect(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句", value = "{'CommandStr':'select * table t where t.xx=1'}", required = true) String param) {
//        //long time1 = System.currentTimeMillis();
//        List<Map<String, Object>> list = new ArrayList<>();
//        JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
//
//        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
//            try {
//                // 通过jdbcTemplate查询数据库
//                list = jdbcTemplate.queryForList(jsonBody.getCommandStr());
//            } catch (Exception e) {
//                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
//                return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage(), ""));
//            }
//
//        }
//        if (list.size() > 0) {
//            String reult = JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(list.size()), list), SerializerFeature.WriteMapNullValue);
//            //long time2 = System.currentTimeMillis();
//            //System.out.println((time2 - time1) + "=======" + reult);
//            return reult;
//        } else {
//            return JSONObject.toJSONString(ResponseHelper.buildResponseResult("2", "0", ""));
//        }
//
//    }
//
//    /**
//     * @MethodName:
//     * @Description: TODO
//     * @Param:
//     * @Return:
//     * @Author: Soft001
//     * @Date: 2020/5/11
//     **/
//    //@ResponseBody
//    @Transactional(propagation = Propagation.REQUIRES_NEW)
//    @RequestMapping(value = "Delete", method = RequestMethod.POST)
//    //@ApiImplicitParam(paramType = "Delete", name = "CommandStr", value = "sql语句", dataType = "string")
//    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
//    @ApiOperation(value = "执行删除操作", notes = "返回删除结果信息{“code”:“0”, “message”:“失败原因：xxx”}", httpMethod = "POST")
//    public String exeDelete(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句", value = "{'CommandStr':'delete table t where t.xx=1'}", required = true) String param) {
//        JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
//        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
//            try {
//                jdbcTemplate.execute(jsonBody.getCommandStr());
//            } catch (Exception e) {
//                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
//                return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage()));
//            }
//
//        }
//        return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS));
//    }
//
//
//    /**
//     * @MethodName:
//     * @Description: TODO
//     * @Param:
//     * @Return:
//     * @Author: Soft001
//     * @Date: 2020/5/11
//     **/
//    //@ResponseBody
//    @Transactional(propagation = Propagation.REQUIRES_NEW)
//    @RequestMapping(value = "Insert", method = RequestMethod.POST)
//    // @ApiImplicitParam(paramType = "Insert", name = "CommandStr", value = "sql语句", dataType = "string")
//    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
//    @ApiOperation(value = "执行新增操作", notes = "返回新增主键信息", httpMethod = "POST")
//    public String exeInsert(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句,CommandBlob:BLOB类型JSON对象的键固定为“BLOB”,对应的值和语句中“?”的个数和顺序相互对应.值为base64", value = "{'CommandStr':'insert into table(id,?) values('1',?.?)','CommandBlob':'[{'BLOB':'ED816AC533CD82F5566A30'},{'BLOB':'ED816AC533CD82F5566A30'}]'}", required = true) String param) {
//        System.out.println(param);
//        JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
//        //String result = Constans.SUCCESS;
//        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
//            System.out.println(jsonBody.getCommandStr());
//
//            try {
//                JSONArray array = JSON.parseArray(jsonBody.getCommandBlob());
//                //int size = array.size();
////                KeyHolder keyHolder = new GeneratedKeyHolder();
////                Connection connection1 = jdbcTemplate.getDataSource()
////                        .getConnection();
//
//                if (array != null) {
//                    jdbcTemplate.update(new PreparedStatementCreator() {
//                        @Override
//                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
//                            PreparedStatement ps = connection.prepareStatement(jsonBody.getCommandStr());// Statement.RETURN_GENERATED_KEYS);, new String[]{"id"}
//                            int size = array.size() + 1;
//                            for (int i = 1; i < size; i++) {
//                                JSONObject jso = (JSONObject) array.get(i - 1);
//                                String key = jso.keySet().iterator().next();
//
//                                DBColumnTypeEnum dbColumnTypeEnum = DBColumnTypeEnum.valueOf(key.toUpperCase());
//                                switch (dbColumnTypeEnum) {
//                                    case INT:
//                                        ps.setInt(i, jso.getIntValue(key));
//                                        break;
//                                    case STRING:
//                                        ps.setString(i, jso.getString(key));
//                                        break;
//                                    case BLOB:
//                                        String base64 = jso.get(key).toString();
//                                        if (StringUtil.isNotEmpty(base64)) {
//                                            byte[] bs = new Base64().decode(base64.replaceAll(" ", ""));
//                                            ps.setObject(i, bs);
//                                        } else {
//                                            ps.setObject(i, new byte[0]);
//                                        }
//                                        break;
//                                    case LONG:
//                                        ps.setLong(i, jso.getLong(key));
//                                        break;
//                                    case DATE:
//                                        ps.setTimestamp(i, jso.getTimestamp(key));
//                                        break;
//                                }
////                                    if (key.equalsIgnoreCase("INT")) {
////                                        int val = jso.getIntValue(key);
////                                        ps.setInt(i, val);
////                                    } else if (key.equals("STRING")) {
////                                        String val = jso.getString(key);
////                                        ps.setString(i, val);
////                                    } else if (key.equals("BLOB")) {
////                                        // base64 = URLDecoder.decode(jso.get("BLOB").toString(), "utf-8");
////                                        String base64 = jso.get("BLOB").toString();
////                                        System.out.println(base64);
////                                        //ps.setString(i,base64);
////                                        //ps.execute();
////                                        if (StringUtil.isNotEmpty(base64)) {
////                                            byte[] bs = new Base64().decode(base64.replaceAll(" ", ""));
////                                            ps.setObject(i, bs);
////                                        } else {
////                                            ps.setObject(i, new byte[0]);
////                                        }
////                                    }
//                            }
//                            return ps;
//                        }
//
//                    });
//                    //System.out.println("id:" + keyHolder.getKey());
//                    //return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(keyHolder.getKey())));
//                } else {
//                    jdbcTemplate.update(new PreparedStatementCreator() {
//                        @Override
//                        public PreparedStatement createPreparedStatement(Connection conn)
//                                throws SQLException {
//                            return conn.prepareStatement(jsonBody.getCommandStr());
//
//                        }
//                    });
//                    //jdbcTemplate.update(jsonBody.getCommandStr());
//                    // System.out.println("id:" + keyHolder.getKey());
//                    // return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(keyHolder.getKey())));
//                }
////                if (!connection1.isClosed()) {
////                    try {
////                        connection1.close();
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////                }
//                return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS));//String.valueOf(keyHolder.getKey())));
//            } catch (Exception e) {
//                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
//                return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage()));
//            }
//        }
//        return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", "没有sql语句"));
//    }
//
//
//    /**
//     * @MethodName:
//     * @Description: TODO
//     * @Param:
//     * @Return:
//     * @Author: Soft001
//     * @Date: 2020/5/11
//     **/
//    //@ResponseBody
//    @ApiOperation(value = "执行事务操作", notes = "返回事务操作结果信息", httpMethod = "POST")
//    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
//    @RequestMapping(value = "DBTransaction", method = RequestMethod.POST)
//    public String exeBatchUpdate(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句,CommandBlob:BLOB类型JSON对象的键固定为“BLOB”,对应的值和更新语句中“?”的个数和顺序相互对应.值为base64", value = "{'CommandStr':'update table set xx='刘德华',xx1=?,xx2=? where  xx3='2020-05-09;delete  from table  where xx='2020-05-09';insert into table(xx,xx) values ('1',?)','CommandBlob':'[[{'BLOB':'ED816AC533CD82F5566A30'},{'BLOB':'ED816AC533CD82F5566A30'}],[],[{'BLOB':'ED816AC533CD82F5566A30'}]]'}", required = true) String param) {
//        JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
//        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
//            String[] sqls = jsonBody.getCommandStr().split(";");
//            Object res = txTemplate.execute(new TransactionCallback<Object>() {
//                @Override
//                public Object doInTransaction(TransactionStatus transactionStatus) {
//                    //Object savepoint = transactionStatus.createSavepoint();
//                    // DML执行
//                    JSONArray array = JSON.parseArray(jsonBody.getCommandBlob());
//                    try {
//                        for (int j = 0; j < sqls.length; j++) {
//                            if (array != null) {
//                                JSONArray ar = (JSONArray) array.get(j);
//                                jdbcTemplate.update(sqls[j], new PreparedStatementSetter() {
//                                    @Override
//                                    public void setValues(PreparedStatement ps) throws SQLException {
//                                        if (ar != null) {
//                                            int size = ar.size() + 1;
//                                            for (int i = 1; i < size; i++) {
//                                                JSONObject jso = (JSONObject) ar.get(i - 1);
//                                                String base64 = "";
//                                                try {
//                                                    // base64 = URLDecoder.decode(jso.get("BLOB").toString(), "utf-8");
//                                                    base64 = jso.get(DBColumnTypeEnum.BLOB.name()).toString();
//                                                    if (StringUtil.isNotEmpty(base64)) {
//                                                        byte[] bs = new Base64().decode(base64);
//                                                        ps.setObject(i, bs);
//                                                    } else {
//                                                        ps.setObject(i, new byte[0]);
//                                                    }
//                                                } catch (Exception e) {
//                                                    LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, jso.get("BLOB").toString() + e.getMessage(), e);
//                                                    ps.setObject(i, new byte[0]);
//                                                }
//
//                                            }
//                                        }
//
//                                    }
//                                });
//                            } else {
//                                jdbcTemplate.update(sqls[j]);
//                            }
//                        }
//                    } catch (Exception e) {
//                        LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
//                        transactionStatus.setRollbackOnly();
//                        // transactionStatus.rollbackToSavepoint(savepoint);
//                        return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage()));
//                    }
//
//                    return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS));
//                }
//            });
//            //System.out.println(String.valueOf(res));
//            return String.valueOf(res);
//        }
//        return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", "没有sql语句"));
//    }
//
//
//    /**
//     * @MethodName:
//     * @Description: 执行更改操作
//     * @Param:
//     * @Return:
//     * @Author: yujian
//     * @Date: 2020/5/11
//     **/
//    //@ResponseBody
//    @Transactional(propagation = Propagation.REQUIRES_NEW)
//    @RequestMapping(value = "Update", method = RequestMethod.POST)
//    //@ApiImplicitParam(paramType = "Update", name = "CommandStr", value = "sql语句", dataType = "string")
//    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
//    @ApiOperation(value = "执行更新操作", notes = "返回更新结果信息", httpMethod = "POST")
//    public String exeUpdate(@RequestBody @ApiParam(name = "json字符串,CommandStr:数据库执行语句,CommandBlob:BLOB类型JSON对象的键固定为“BLOB”,对应的值和语句中“?”的个数和顺序相互对应.值为base64", value = "{'CommandStr':'update table t set t.xx=1,t.blob1=?,t.blob2=? where xx=1','CommandBlob':'[{'BLOB':'ED816AC533CD82F5566A30'},{'BLOB':'ED816AC533CD82F5566A30'}]'}", required = true) String param) {
//        JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
//        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
//            try {
//                JSONArray array = JSON.parseArray(jsonBody.getCommandBlob());
//                //int size = array.size();
//                if (array != null) {
//                    jdbcTemplate.update(jsonBody.getCommandStr(), new PreparedStatementSetter() {
//                        @Override
//                        public void setValues(PreparedStatement ps) throws SQLException {
//                            int size = array.size() + 1;
//                            for (int i = 1; i < size; i++) {
//                                JSONObject jso = (JSONObject) array.get(i - 1);
//                                try {
//                                    //base64 = URLDecoder.decode(jso.get("BLOB").toString(), "utf-8");
//                                    String base64 = jso.get(DBColumnTypeEnum.BLOB.name()).toString();
//                                    if (StringUtil.isNotEmpty(base64)) {
//                                        byte[] bs = new Base64().decode(base64);
//                                        ps.setObject(i, bs);
//                                    } else {
//                                        ps.setObject(i, new byte[0]);
//                                    }
//                                } catch (Exception e) {
//                                    LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, jso.get("BLOB").toString() + e.getMessage(), e);
//                                    ps.setObject(i, new byte[0]);
//                                }
//
//                            }
//                        }
//                    });
//                } else {
//                    int res = jdbcTemplate.update(jsonBody.getCommandStr());
//                    System.out.println(res + " ========================");
//                }
//            } catch (Exception e) {
//                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
//                return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage()));
//            }
//
//        }
//        return JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", Constans.SUCCESS));
//    }
//
//    @ApiOperationSupport(author = "三联交通")
//    @RequestMapping(value = "Callable", method = RequestMethod.POST)
//    //@ApiImplicitParam(paramType = "body", name = "CommandStr", value = "sql语句", dataType = "string")
//    @ApiOperation(value = "执行存储过程操作", notes = "返回存储过程信息", httpMethod = "POST")
//    @ApiResponses({@ApiResponse(code = 200, message = "返回json字符串", response = RespBody.class)})
//    @SuppressWarnings("unchecked")
//    public String exeCallable(@RequestBody @ApiParam(name = "CommandStr：存储过程名称入参出参用'?'代替，CommandBlob：入参Key包括（FLOAT,DOUBLE,INT, STRING, BLOB,DATE,LONG);出参key包括（OUTCURSOR,OUNUMBER,OUTSTRING,OUTDOUBLE,OUTFLOAT）,value为空串", value = "{'CommandStr':'p_getAllKSCJ(?,?,?)','CommandBlob':'[{'INT',1},{'STRING':'2'},{'OUTCURSOR':''}]'", required = true) String param) {
//        //long time1 = System.currentTimeMillis();
//        List<Map<String, Object>> list = new ArrayList<>();
//        //JSONObject jsonObject = JSON.parseObject(param);
//        JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
//        if (StringUtil.isNotEmpty(jsonBody.getCommandStr())) {
//            try {
//                JSONArray intarray = JSON.parseArray(jsonBody.getCommandBlob());
//                // 通过jdbcTemplate查询数据库
//                list = (List<Map<String, Object>>) jdbcTemplate.execute(
//                        new CallableStatementCreator() {
//                            public CallableStatement createCallableStatement(Connection con) throws SQLException {
//                                String storedProc = "{call " + jsonBody.getCommandStr() + "}";// 调用的sql
//                                CallableStatement cs = con.prepareCall(storedProc);
//                                if (intarray != null) {
//                                    int size = intarray.size() + 1;
//                                    for (int i = 1; i < size; i++) {
//                                        JSONObject jso = (JSONObject) intarray.get(i - 1);
//                                        Iterator iterator = jso.keySet().iterator();
//                                        String key = (String) iterator.next();
//                                        DBColumnTypeEnum dbColumnTypeEnum = DBColumnTypeEnum.valueOf(key.toUpperCase());
//                                        switch (dbColumnTypeEnum) {
//                                            case INT:
//                                                cs.setInt(1, jso.getIntValue(key));// 设置输入参数的值
//                                                break;
//                                            case STRING:
//                                                cs.setString(i, jso.getString(key));
//                                                break;
//                                            case BLOB:
//                                                String base64 = jso.getString(key);
//                                                if (StringUtil.isNotEmpty(base64)) {
//                                                    byte[] bs = new Base64().decode(base64.replaceAll(" ", ""));
//                                                    cs.setObject(i, bs);
//                                                } else {
//                                                    cs.setObject(i, new byte[0]);
//                                                }
//                                                break;
//                                            case LONG:
//                                                cs.setLong(i, jso.getLongValue(key));
//                                                break;
//                                            case FLOAT:
//                                                cs.setFloat(i, jso.getFloatValue(key));
//                                                break;
//                                            case DOUBLE:
//                                                cs.setDouble(i, jso.getDoubleValue(key));
//                                                break;
//                                            case DATE:
//                                                cs.setTimestamp(i, jso.getTimestamp(key));
//                                                break;
//                                            case OUTCURSOR:
//                                                cs.registerOutParameter(i, OracleTypes.CURSOR);
//                                                break;
//                                            case OUNUMBER:
//                                                cs.registerOutParameter(i, OracleTypes.NUMBER);
//                                                break;
//                                            case OUTDOUBLE:
//                                                cs.registerOutParameter(i, OracleTypes.DOUBLE);
//                                                break;
//                                            case OUTFLOAT:
//                                                cs.registerOutParameter(i, OracleTypes.FLOAT);
//                                                break;
//                                            case OUTSTRING:
//                                                cs.registerOutParameter(i, OracleTypes.VARCHAR);
//                                                break;
//                                        }
//                                    }
//
//                                }
//                                return cs;
//                            }
//                        }, new CallableStatementCallback() {
//                            public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
//                                List resultsMap = new ArrayList();
//                                cs.execute();
//                                ResultSet rs = (ResultSet) cs.getObject(2);// 获取游标一行的值
//                                while (rs.next()) {// 转换每行的返回值到Map中
//                                    //System.out.println("------------------------");
//                                    Map<String, Object> rowMap = new HashMap();
//                                    int columnCount = rs.getMetaData().getColumnCount();
//                                    for (int i = 1; i <= columnCount; i++) {
//                                        // System.out.println(rs.getMetaData().getColumnName(i));
//                                        rowMap.put(rs.getMetaData().getColumnName(i), rs.getObject(rs.getMetaData().getColumnName(i)));
////                                        if (rs.getMetaData().getColumnType(i)==Types.BLOB){
////                                            rowMap.put(rs.getMetaData().getColumnName(i), rs.getBytes(rs.getMetaData().getColumnName(i)));
////                                        }
//                                    }
//                                    resultsMap.add(rowMap);
//                                }
//                                rs.close();
//                                return resultsMap;
//                            }
//                        });
//            } catch (Exception e) {
//                LogExeManager.getInstance().executeLogTask(LogType.ERROR, Constans.FOUR_STRING, e.getMessage(), e);
//                return JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", e.getMessage(), ""));
//            }
//
//        }
//        if (list.size() > 0) {
//            String result = JSONObject.toJSONString(ResponseHelper.buildResponseResult("1", String.valueOf(list.size()), list), new ValueFilter() {
//                @Override
//                public Object process(Object obj, String s, Object v) {
//                    if (v == null)
//                        return "";
//                    return v;
//                }
//            });
//            //long time2 = System.currentTimeMillis();
//            //System.out.println((time2 - time1) + "=======" + reult);
//            return result;
//        } else {
//            return JSONObject.toJSONString(ResponseHelper.buildResponseResult("2", "0", ""));
//        }
//
//    }
//}
