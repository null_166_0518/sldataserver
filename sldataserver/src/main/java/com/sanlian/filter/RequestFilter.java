package com.sanlian.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sanlian.portal.config.ResponseHelper;
import com.sanlian.portal.datasource.ShardingConfiguration;
import com.sanlian.portal.entity.JsonBody;
import com.sanlian.portal.task.TaskRunnableFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName RequestFilter
 * @Description: TODO
 * @Author Soft001
 * @Date 2020/9/15
 **/
@Component
public class RequestFilter implements Filter {
    @Autowired
    ShardingConfiguration shardingConfiguration;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        StringBuffer sbf = new StringBuffer();
        BufferedReader reader = null;
        try {
            HttpServletRequest req = (HttpServletRequest) request;
            reader = new BufferedReader(new InputStreamReader(req.getInputStream()));
            String lines;
            while ((lines = reader.readLine()) != null) {
                lines = new String(lines.getBytes(), "utf-8");
                sbf.append(lines);
            }
            String param = sbf.toString();
            JsonBody jsonBody = JSON.parseObject(param, JsonBody.class);
            jsonBody.setKeyNum();
            if (!TaskRunnableFactory.watchRequests.containsKey(jsonBody.getKeyNum())) {
                //DeferredResult<String> deferredResult = new DeferredResult<String>(shardingConfiguration.getTransactionTimeout());
                //deferredResult.setResult(JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", "请勿重复请求", "")));
                //TaskRunnableFactory.watchRequests.put(jsonBody.getKeyNum(), null);
                OracleReqParamWrapper wrapper = new OracleReqParamWrapper(req, jsonBody);
                chain.doFilter(wrapper, response);
            } else {
                handler401(response,"请勿重复请求" );
            }

        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    private void handler401(ServletResponse response, String msg) {
        try {
            String res = JSONObject.toJSONString(ResponseHelper.buildResponseResult("0", msg, ""));
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setStatus(HttpStatus.OK.value());
            response.setContentType("application/json;charset=utf-8");
            // httpResponse.getWriter().write("{\"code\":" + code + ", \"message\":\"" + msg + "\"}");
            //String res="{\"code\":" + code + ", \"message\":\"" + msg + "\"}";
            ServletOutputStream out = httpResponse.getOutputStream();
            out.write(res.getBytes("utf8"));
            out.flush();
            out.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
