package com.sanlian.filter;

import com.alibaba.fastjson.JSON;
import com.sanlian.portal.entity.JsonBody;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * @ClassName OracleReqParamWrapper
 * @Description: 将请求参数写入body
 * @Author Soft001
 * @Date 2020/9/15
 **/
public class OracleReqParamWrapper extends HttpServletRequestWrapper {
    JsonBody reqBodyStr = null;

    public OracleReqParamWrapper(HttpServletRequest request, JsonBody reqBodyStr) {
        super(request);
        // 由于我们只做POST请求, 所以这里不做任何处理
        this.reqBodyStr = reqBodyStr;
    }

    /**
     * 重写getInputStream方法
     */
    @Override
    public ServletInputStream getInputStream() throws IOException {
        //非json类型，直接返回
        if (!super.getHeader(HttpHeaders.CONTENT_TYPE).equalsIgnoreCase(MediaType.APPLICATION_JSON_VALUE)) {
            return super.getInputStream();
        }
        ByteArrayInputStream bis = null;
        try {
            //从输入流中取出body串, 如果为空，直接返回
            if (reqBodyStr == null) {
                return super.getInputStream();
            }
            //重新构造一个输入流对象
            byte[] bytes = JSON.toJSONString(reqBodyStr).getBytes("utf-8");
            bis = new ByteArrayInputStream(bytes);
        } finally {
            if (bis != null) {
                bis.close();
            }
        }

        return new OracleServletInputStream(bis);
    }

    static class OracleServletInputStream extends ServletInputStream {

        private ByteArrayInputStream bis;

        public OracleServletInputStream(ByteArrayInputStream bis) {
            this.bis = bis;
        }

        @Override
        public boolean isFinished() {
            return true;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener listener) {

        }

        @Override
        public int read() throws IOException {
            return bis.read();
        }
    }
}
