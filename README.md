# sldataserver

#### 介绍
用于oracle数据库代理的中间件，采用http接口交互执行sql

#### 软件架构
springboot项目,jdk1.8
##### 主从库配置 在application.properties
```xml
#主库ip
sharding.Masterip=192.168.200.108
#实例名
sharding.MasterServiceName=SLEXAM
#y用户名
sharding.MasterUseranme=ROADEXAM
#密码
sharding.MasterPassword=SLJTYY_888888
#IP
sharding.Slaveip=172.30.1.209
#实例名
sharding.SlaveServiceName=SLEXAM
#y用户名
sharding.SlaveUseranme=ROADEXAM
#密码
sharding.SlavePassword=SLJTYY_888888

#是否切换从库1.不切，2.切换
sharding.SingletonMasterToSlave=1

#Master代表单库，MasterToSlave代表主从双库
spring.profiles.active=MasterToSlave

#事务请求超时时间单位毫秒
sharding.TransactionTimeout=10000
```
####### 按月分表
```xml
1.在主从库将需要分表的表，规则按照：表名_序号。例如：b_ks_0,b_ks_1,b_ks_2....分表数最多到b_ks_11。
2.添加如下配置到application.properties
```
```xml
#分表序号,例如B_KS_0,B_KS_1
sharding.tables.B_KS.actual_table_nodes=0..1
#分表字段名称，当sql条件中包含此字段，回根据字段自动查找对应分表，不包括则查询所有分表
sharding.tables.B_KS.sharding_column=ykrq
#分表字段月份2种格式1.yyyy-MM-dd,2.yyyy-MM-ddHH:mm:ss
sharding.tables.B_KS.column_rule=yyyy-MM-dd
```
#### 使用说明
系统启动后，浏览器输入http://localhost:9604/doc.html，进行查看接口文档

